package com.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.CommentDAO;
import com.epam.newsmng.domain.Comment;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@DbUnitConfiguration(databaseConnection = { "dataSource", "dbUnitDatabaseConnection" })
@ContextConfiguration(locations = { "classpath:/**/applicationConfigTest.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class})

@Transactional
public class CommentDAOTest extends AbstractDAOTest {

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(CommentDAOTest.class);

	private static final String NEWS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_table_data.xml";
	
	private static final String COMMENTS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/comment_table_data.xml";

	@Autowired
	private CommentDAO commentDAO;

	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(COMMENTS_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}

	@Test
	public void insertComment() {
		Comment comment = new Comment("Third", LocalDateTime.of(2016, 04, 20, 12, 10, 30, 0));
		comment.setNewsId(new Long(3));
		long id = commentDAO.save(comment);
		Assert.assertEquals(7L, id);
	}

	/*@Test
	public void selectCommentById() {
		Long commentId = new Long(1);
		Comment commentExpected = new Comment(commentId, new Long(1), "Text1", Timestamp.valueOf("2016-04-20 10:10:30.12"));
		Comment commentRecieved = commentDAO.retrieveById(commentId);
		Assert.assertEquals(commentExpected, commentRecieved);
	}

	@Test
	public void selectAllComments() {
		List<Comment> listOfComments = null;
		listOfComments = (List<Comment>) commentDAO.retrieveAll();
		Assert.assertEquals(6, listOfComments.size());
	}

	@Test
	public void updateComment() {
		Comment commentToUpdate = new Comment(new Long(1), new Long(1), "Text111111", Timestamp.valueOf("2016-04-20 10:10:30.12"));
		commentDAO.update(commentToUpdate);
		Comment commentRecieved = commentDAO.retrieveById(commentToUpdate.getCommentId());
		Assert.assertEquals(commentToUpdate, commentRecieved);
	}*/

	/*@Test
	public void deleteCommentById() {
		Long commentId = new Long(1);
		commentDAO.deleteById(commentId);
		/*Comment commentRecieved = commentDAO.selectById(commentId);
		Assert.assertEquals(commentRecieved, new Comment(null, null, null, null));
	}

	@Test
	public void selectAllCommentsByNewsId() {
		Long newsId = new Long(2);
		List<Comment> listOfCommentsExpected = new ArrayList<Comment>();
		
		Comment com1 = new Comment(new Long(2), newsId, "Text2", Timestamp.valueOf("2016-04-20 10:15:30.19"));
		Comment com2 = new Comment(new Long(3), newsId, "Text3", Timestamp.valueOf("2016-04-20 15:10:30.11"));
		listOfCommentsExpected.add(com1); listOfCommentsExpected.add(com2);
		
		List<Comment> listOfCommentsRecieved = (List<Comment>) commentDAO.retrieveAllCommentsByNewsId(newsId);
		Assert.assertEquals(listOfCommentsExpected, listOfCommentsRecieved);
	}

	@Test
	public void updateCommentWithCensuring() {
		commentDAO.updateCommentWithCensuring(new Long(1), "Some reason");
		String newString = "НЛО прилетело и удалило эту запись! Some reason";
		
		Comment commentExpected = new Comment(new Long(1), new Long(1), newString, Timestamp.valueOf("2016-04-20 10:10:30.12"));
		Comment commentRecieved = commentDAO.retrieveById(new Long(1));
		
		Assert.assertEquals(commentExpected, commentRecieved);
	}

	@Test
	public void deleteCommentFromNews() {
		Long commentId = new Long(1);
		Long newsId = new Long(1);
		commentDAO.deleteCommentFromNews(newsId, commentId);
		Comment commentRecieved = commentDAO.retrieveById(commentId);
		Assert.assertEquals(commentRecieved.getCommentId(), null);
	}

	@Test
	public void deleteAllCommentsFromNews() {
		Long newsId = new Long(1);
		commentDAO.deleteAllCommentsFromNews(newsId);
		Collection<Comment> commentsRecieved = commentDAO.retrieveAllCommentsByNewsId(newsId);
		Assert.assertEquals(commentsRecieved, new ArrayList<Comment>());
	}
	
	@Test
	public void selectCommentByNewsId() {
		List<Long> listOfNewsIdExpected = new ArrayList<Long>();
		List<Long> listOfNewsIdRecieved = new ArrayList<Long>();
		
		listOfNewsIdExpected.add(new Long(3));
		listOfNewsIdExpected.add(new Long(2));
		listOfNewsIdRecieved = (List<Long>) commentDAO.retrieveNMostCommentedNewsId(2);
		Assert.assertEquals(listOfNewsIdExpected, listOfNewsIdRecieved);
	}
	
	@Test
	public void selectCommentsByEachNewsId() {
		Map<Long, Collection<Comment>> mapOfNewsIdsAndCommentsExpected = new LinkedHashMap<Long, Collection<Comment>>();
		Map<Long, Collection<Comment>> mapOfNewsIdsAndCommentsRecieved = null;
		
		Long newsId1 = new Long(1);
		Long newsId2 = new Long(2);
		Collection<Long> newsIds = new ArrayList<Long>();
		newsIds.add(newsId1); newsIds.add(newsId2);
		
		Comment comment1 = new Comment(new Long(1), new Long(1), "Text1", Timestamp.valueOf("2016-04-20 10:10:30.12"));
		Comment comment2 = new Comment(new Long(2), new Long(2), "Text2", Timestamp.valueOf("2016-04-20 10:15:30.19"));
		Comment comment3 = new Comment(new Long(3), new Long(2), "Text3", Timestamp.valueOf("2016-04-20 15:10:30.11"));
		
		List<Comment> authorCollection1 = new ArrayList<Comment>();
		List<Comment> authorCollection2 = new ArrayList<Comment>();
		
		authorCollection1.add(comment1);
		authorCollection2.add(comment2); authorCollection2.add(comment3);
		
		mapOfNewsIdsAndCommentsExpected.put(newsId1, authorCollection1);
		mapOfNewsIdsAndCommentsExpected.put(newsId2, authorCollection2);
		
		mapOfNewsIdsAndCommentsRecieved = commentDAO.retrieveCommentsByEachNewsId(newsIds);
		Assert.assertEquals(mapOfNewsIdsAndCommentsExpected, mapOfNewsIdsAndCommentsRecieved);
	}*/
}
