package com.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.AuthorDAO;
import com.epam.newsmng.domain.Author;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@DbUnitConfiguration(databaseConnection = { "dataSource", "dbUnitDatabaseConnection" })
@ContextConfiguration(locations = { "classpath:/**/applicationConfigTest.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class})

@Transactional
public class AuthorDAOTest extends AbstractDAOTest {
	
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(AuthorDAOTest.class);
	
	private static final String AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/author_table_data.xml";
	
	@Autowired
	private AuthorDAO authorDAO;

	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = {
				new FlatXmlDataSetBuilder().build(new FileInputStream(AUTHOR_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}

	@Test
	public void insertAuthor() {
		Author author = new Author();
		author.setAuthorName("Petrov");
		long id = authorDAO.save(author);
		Assert.assertEquals(4L, id);
	}

	/*@Test
	public void selectAuthorById() {
		Long authorId = new Long(1);
		Author authorExpected = new Author(authorId, "Pushkin", null);
		Author authorRecieved = authorDAO.retrieveById(authorId);
		Assert.assertEquals(authorExpected, authorRecieved);
	}
	
	@Test
	public void selectAllAuthors() {
		List<Author> listOfAuthors = null;
		listOfAuthors = (List<Author>) authorDAO.retrieveAll();
		Assert.assertEquals(3, listOfAuthors.size());
	}

	@Test
	public void updateAuthor() {
		Author authorToUpdate = new Author(new Long(1), "Aleksander Pushkin", null);
		authorDAO.update(authorToUpdate);
		Author authorRecieved = authorDAO.retrieveById(authorToUpdate.getAuthorId());
		Assert.assertEquals(authorToUpdate, authorRecieved);
	}*/

	/*@Test(expected = DAOException.class)
	public void deleteAuthorById() {
		Long authorId = new Long(1);
		authorDAO.deleteById(authorId);
		/*Author authorRecieved = authorDAO.selectById(authorId);
		Assert.assertNotEquals(authorRecieved, new Author(null, null, null));
	}

	@Test
	public void makeAuthorExpired() {
		Timestamp tstmp = new Timestamp(System.currentTimeMillis());
		Author authorExpected = new Author(new Long(1), "Pushkin", tstmp);
		
		authorDAO.retrieveAuthorExpiredById(new Long(1), tstmp);
		Author authorRecieved = authorDAO.retrieveById(authorExpected.getAuthorId());
		Assert.assertEquals(authorExpected, authorRecieved);
	}

	@Test
	public void selectAuthorByName() {
		String authorName = "Pushkin";
		Author authorExpected = new Author(new Long(1), "Pushkin", null);
		Author authorRecieved = authorDAO.retrieveByName(authorName);
		Assert.assertEquals(authorExpected, authorRecieved);
	}

	@Test
	public void selectExistingAuthorsByNames() {
		Author author1 = new Author(new Long(1), "Pushkin", null);
		Author author2 = new Author(new Long(3), "Gogol", null);
		List<Author> collectionExpected = new ArrayList<Author>();
		collectionExpected.add(author1);
		collectionExpected.add(author2);
		List<String> authorNames = new ArrayList<String>();
		authorNames.add("Pushkin");
		authorNames.add("Gogol");
		List<Author> collectionRecieved = (List<Author>) authorDAO.retrieveExistingAuthorsByNames(authorNames);
		Assert.assertEquals(collectionExpected, collectionRecieved);
	}

	@Test
	public void insertAllAuthors() {
		Author author1 = new Author("Ivanov", null);
		Author author2 = new Author("Petrov", null);
		Author author3 = new Author("Sidorov", null);
		List<Author> authors = new ArrayList<Author>();
		authors.add(author1); authors.add(author2); authors.add(author3);
		
		authorDAO.saveAllAuthors(authors);
		author1.setAuthorId(new Long(4));
		author2.setAuthorId(new Long(5));
		author3.setAuthorId(new Long(6));
		
		List<String> authorsName = new ArrayList<String>();
		authorsName.add("Ivanov");
		authorsName.add("Petrov");
		authorsName.add("Sidorov");
		List<Author> authorsRecieved = (List<Author>) authorDAO.retrieveExistingAuthorsByNames(authorsName);
		
		Assert.assertEquals(authorsRecieved, authors);
	}

	@Test
	public void insertAuthorsToNews() {
		Author author1 = new Author("Pushkin", null);
		Author author2 = new Author("Lermontov", null);
		Author author4 = new Author("Ivanov", null);
		Author author5 = new Author("Petrov", null);
		Author author6 = new Author("Sidorov", null);
		
		List<Author> authors = new ArrayList<Author>();
		authors.add(author4); authors.add(author5); authors.add(author6);
		authorDAO.saveAllAuthors(authors);
		
		author1.setAuthorId(new Long(1));
		author2.setAuthorId(new Long(2));
		author4.setAuthorId(new Long(4));
		author5.setAuthorId(new Long(5));
		author6.setAuthorId(new Long(6));
		
		List<Long> listOfAuthorIds = new ArrayList<Long>();
		listOfAuthorIds.add(new Long(4));
		listOfAuthorIds.add(new Long(5));
		listOfAuthorIds.add(new Long(6));
		authorDAO.saveNewsAuthors(new Long(1), listOfAuthorIds);
		
		authors.clear();
		authors.add(author1); authors.add(author2); authors.add(author4); authors.add(author5); authors.add(author6);
		List<Author> authorsRecieved = (List<Author>) authorDAO.retrieveAuthorsByNewsId(new Long(1));
		
		Assert.assertEquals(authors, authorsRecieved);
	}

	@Test
	public void selectAuthorsByNewsId() {
		Author author1 = new Author(new Long(1), "Pushkin", null);
		Author author2 = new Author(new Long(2), "Lermontov", null);
		
		List<Author> authors = new ArrayList<Author>();
		authors.add(author1);
		authors.add(author2);
	
		List<Author> authorsRecieved = (List<Author>) authorDAO.retrieveAuthorsByNewsId(new Long(1));
		Assert.assertEquals(authors, authorsRecieved);
	}

	@Test
	public void selectAuthorsByEachNewsId() {
		Map<Long, Collection<Author>> mapOfNewsIdsAndAuthorsExpected = new LinkedHashMap<Long, Collection<Author>>();
		Map<Long, Collection<Author>> mapOfNewsIdsAndAuthorsRecieved = null;
		
		Long newsId1 = new Long(1);
		Long newsId2 = new Long(2);
		Long newsId3 = new Long(3);
		Collection<Long> newsIds = new ArrayList<Long>();
		newsIds.add(newsId1); newsIds.add(newsId2); newsIds.add(newsId3);
		
		Author author1 = new Author(new Long(1), "Pushkin", null);
		Author author2 = new Author(new Long(2), "Lermontov", null);
		Author author3 = new Author(new Long(3), "Gogol", null);
		
		List<Author> authorCollection1 = new ArrayList<Author>();
		List<Author> authorCollection2 = new ArrayList<Author>();
		List<Author> authorCollection3 = new ArrayList<Author>();
		
		authorCollection1.add(author1); authorCollection1.add(author2);
		authorCollection2.add(author1); authorCollection2.add(author2); authorCollection2.add(author3);
		authorCollection3.add(author2); authorCollection3.add(author3);
		
		mapOfNewsIdsAndAuthorsExpected.put(newsId1, authorCollection1);
		mapOfNewsIdsAndAuthorsExpected.put(newsId2, authorCollection2);
		mapOfNewsIdsAndAuthorsExpected.put(newsId3, authorCollection3);
		
		mapOfNewsIdsAndAuthorsRecieved = authorDAO.retrieveAuthorsByEachNewsId(newsIds);
		Assert.assertEquals(mapOfNewsIdsAndAuthorsExpected, mapOfNewsIdsAndAuthorsRecieved);
	}

	@Test
	public void deleteAuthorFromNews() {
		Author author = new Author(new Long(3), "Gogol", null);
		authorDAO.deleteAuthorFromNews(author.getAuthorId(), new Long(2));
		
		List<Author> authorsExpected = new ArrayList<Author>();
		authorsExpected.add(author);
	
		List<Author> authorsRecieved = (List<Author>) authorDAO.retrieveAuthorsByNewsId(new Long(3));
		Assert.assertEquals(authorsExpected, authorsRecieved);
	}

	@Test
	public void deleteAllAuthorsFromNews() {
		authorDAO.deleteAllAuthorsFromNews(new Long(3));
		
		List<Author> authorsExpected = new ArrayList<Author>();
	
		List<Author> authorsRecieved = (List<Author>) authorDAO.retrieveAuthorsByNewsId(new Long(3));
		Assert.assertEquals(authorsExpected, authorsRecieved);
	}*/
}
