package com.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDAOTest {
	
	private static final String DELETE_ALL_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/delete_data.xml";
	
	public static final DatabaseOperation SEQUENCE_RESETTER_ORACLE = new DatabaseOperation() {
	    @Override
	    public void execute(IDatabaseConnection connection, IDataSet dataSet) throws DatabaseUnitException, SQLException {
	        String[] tables = dataSet.getTableNames();
	        Statement statement = connection.getConnection().createStatement();
	        for (String table : tables) {
	        	if (!table.contains("_")) {	        		
		        	int startWith = dataSet.getTable(table).getRowCount() + 1;
		        	statement.execute("drop sequence " + table + "_SEQ");
		            statement.execute("create sequence " + table + "_SEQ START WITH " + startWith);
	        	}
	        }
	    }
	};
	
	@Autowired
	private DataSource dataSource;
	
	private Connection con;

	public AbstractDAOTest() {
		Locale.setDefault(Locale.ENGLISH);
	}
	
	public IDatabaseConnection takeConnection() throws SQLException, DatabaseUnitException {
		con = dataSource.getConnection();
		DatabaseMetaData dbMeta = con.getMetaData();
		IDatabaseConnection connection = new DatabaseConnection(con, dbMeta.getUserName());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
		return connection;
	}
	
	public abstract void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException;
	
	public abstract IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException;

	public IDataSet loadDataSetToDelete() throws FileNotFoundException, DataSetException {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(DELETE_ALL_DATA_XML_FILE_LOCATION));
		return dataSet;
	}
	
	// This method should be commented if we are going to use @ExpectedDatabase annotation in test
	// because it works before the annotation could check the results
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToDelete());
		releaseConnection();
	}

	@After
	public void releaseConnection() throws SQLException {
		con.close();
	}	
}