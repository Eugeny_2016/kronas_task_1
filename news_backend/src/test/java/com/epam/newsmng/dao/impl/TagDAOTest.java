package com.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.TagDAO;
import com.epam.newsmng.domain.Tag;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@DbUnitConfiguration(databaseConnection = { "dataSource", "dbUnitDatabaseConnection" })
@ContextConfiguration(locations = { "classpath:/**/applicationConfigTest.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class})

@Transactional
public class TagDAOTest extends AbstractDAOTest {
	
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(TagDAOTest.class);
	
	private static final String TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/tag_table_data.xml";

	@Autowired
	private TagDAO tagDAO;

	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(TAG_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}

	@Test
	public void insertTag() {
		Tag expectedTag = new Tag("Tag5");
		Tag actualTag = tagDAO.persist(expectedTag);
		Assert.assertEquals(actualTag.getTagName(), expectedTag.getTagName());
	}

	/*@Test
	public void selectTagById() {
		Tag tagExpected = new Tag(new Long(4), "history");
		Tag tagRecieved = tagDAO.retrieveById(new Long(4));
		Assert.assertEquals(tagExpected, tagRecieved);
	}

	@Test
	public void selectAllTags() {
		List<Tag> listOfTags = null;
		listOfTags = (List<Tag>) tagDAO.retrieveAll();
		Assert.assertEquals(4, listOfTags.size());
	}
	
	@Test
	public void updateTag() {
		Tag tagToUpdate = new Tag(new Long(4), "pistory");
		tagDAO.update(tagToUpdate);
		Tag tagRecieved = tagDAO.retrieveById(tagToUpdate.getTagId());
		Assert.assertEquals(tagToUpdate, tagRecieved);
	}

	@Test(expected = DAOException.class)
	public void deleteTagById() {
		Long tagId = new Long(1);
		tagDAO.deleteById(tagId);
		/*Tag tagRecieved = tagDAO.selectById(tagId);
		Assert.assertEquals(tagRecieved, new Tag(null, null));
	}

	@Test
	public void selectAllTagsByNewsId() {
		Tag tag1 = new Tag(new Long(3), "18 vek");
		Tag tag2 = new Tag(new Long(4), "history");
		
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag1);
		tags.add(tag2);
	
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.retrieveAllTagsByNewsId(new Long(3));
		Assert.assertEquals(tags, tagsRecieved);
	}

	@Test
	public void selectExistingTagsByNames() {
		List<String> listOfTagNames = new ArrayList<String>();
		listOfTagNames.add("18 vek"); listOfTagNames.add("not existing tag"); listOfTagNames.add("history");
		
		List<Tag> listOfTagsExpected = new ArrayList<Tag>();
		Tag tag1 = new Tag(new Long(3), "18 vek");
		Tag tag2 = new Tag(new Long(4), "history");
		listOfTagsExpected.add(tag1); listOfTagsExpected.add(tag2);
		
		List<Tag> listOfTagsRecieved = (List<Tag>) tagDAO.retrieveExistingTagsByNames(listOfTagNames);
		Assert.assertEquals(listOfTagsExpected, listOfTagsRecieved);
	}
	
	@Test
	public void insertAllTags() {
		Tag tag1 = new Tag("Frist");
		Tag tag2 = new Tag("Second");
		Tag tag3 = new Tag("Third");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag1); tags.add(tag2); tags.add(tag3);
		
		tagDAO.saveAllTags(tags);
		tag1.setTagId(new Long(5));
		tag2.setTagId(new Long(6));
		tag3.setTagId(new Long(7));
		
		List<String> tagsName = new ArrayList<String>();
		tagsName.add("Frist");
		tagsName.add("Second");
		tagsName.add("Third");
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.retrieveExistingTagsByNames(tagsName);
		
		Assert.assertEquals(tagsRecieved, tags);
	}
		
	@Test
	public void insertTagsToNews() {
		Tag tag1 = new Tag("18 vek");
		Tag tag2 = new Tag("history");
		Tag tag3 = new Tag("First");
		Tag tag4 = new Tag("Second");
		
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag3); tags.add(tag4);
		tagDAO.saveAllTags(tags);
		
		tag1.setTagId(new Long(3));
		tag2.setTagId(new Long(4));
		tag3.setTagId(new Long(5));
		tag4.setTagId(new Long(6));
		
		List<Long> listOfTagIds = new ArrayList<Long>();
		listOfTagIds.add(new Long(5));
		listOfTagIds.add(new Long(6));
		tagDAO.saveNewsTags(new Long(3), listOfTagIds);
		
		tags.clear();
		tags.add(tag1); tags.add(tag2); tags.add(tag3); tags.add(tag4);
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.retrieveAllTagsByNewsId(new Long(3));
		
		Assert.assertEquals(tags, tagsRecieved);
	}

	@Test
	public void selectCommentsByEachNewsId() {
		Map<Long, Collection<Tag>> mapOfNewsIdsAndTagsExpected = new LinkedHashMap<Long, Collection<Tag>>();
		Map<Long, Collection<Tag>> mapOfNewsIdsAndTagsRecieved = null;
		
		Long newsId1 = new Long(1);
		Long newsId2 = new Long(2);
		Long newsId3 = new Long(3);
		Collection<Long> newsIds = new ArrayList<Long>();
		newsIds.add(newsId1); newsIds.add(newsId2); newsIds.add(newsId3);
		
		Tag tag1 = new Tag(new Long(1), "russian classical literature");
		Tag tag2 = new Tag(new Long(2), "mertvie dushi");
		Tag tag3 = new Tag(new Long(3), "18 vek");
		Tag tag4 = new Tag(new Long(4), "history");
		
		List<Tag> tagCollection1 = new ArrayList<Tag>();
		List<Tag> tagCollection2 = new ArrayList<Tag>();
		List<Tag> tagCollection3 = new ArrayList<Tag>();
		
		tagCollection1.add(tag1);
		tagCollection2.add(tag2);
		tagCollection3.add(tag3); tagCollection3.add(tag4);
		
		mapOfNewsIdsAndTagsExpected.put(newsId1, tagCollection1);
		mapOfNewsIdsAndTagsExpected.put(newsId2, tagCollection2);
		mapOfNewsIdsAndTagsExpected.put(newsId3, tagCollection3);
		
		mapOfNewsIdsAndTagsRecieved = tagDAO.retrieveTagsByEachNewsId(newsIds);
		Assert.assertEquals(mapOfNewsIdsAndTagsExpected, mapOfNewsIdsAndTagsRecieved);
	}

	@Test
	public void deleteTagFromNews() {
		Tag tag = new Tag(new Long(3), "18 vek");
		tagDAO.deleteTagFromNews(new Long(3), new Long(4));
		
		List<Tag> tagsExpected = new ArrayList<Tag>();
		tagsExpected.add(tag);
	
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.retrieveAllTagsByNewsId(new Long(3));
		Assert.assertEquals(tagsExpected, tagsRecieved);
	}

	@Test
	public void deleteAlTagsFromNews() {
		Long newsId = new Long(3);
		tagDAO.deleteAllTagsFromNews(newsId);
		Collection<Tag> commentsRecieved = tagDAO.retrieveAllTagsByNewsId(newsId);
		Assert.assertEquals(commentsRecieved, new ArrayList<Tag>());
	}*/
}
