package com.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.NewsDAO;
import com.epam.newsmng.domain.Author;
import com.epam.newsmng.domain.Comment;
import com.epam.newsmng.domain.News;
import com.epam.newsmng.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@DbUnitConfiguration(databaseConnection = { "dataSource", "dbUnitDatabaseConnection" })
@ContextConfiguration(locations = { "classpath:/**/applicationConfigTest.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class})

@Transactional
public class NewsDAOTest extends AbstractDAOTest {

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(NewsDAOTest.class);

	private static final String NEWS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_table_data.xml";

	private static final String AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/author_table_data.xml";

	private static final String NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_author_table_data.xml";

	private static final String TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/tag_table_data.xml";

	private static final String NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_tag_table_data.xml";

	private static final String COMMENT_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/comment_table_data.xml";

	private static final String EXPECTED_AFTER_NEWS_INSERTION_DATASET_FILE_LOCATION = "classpath:db_data/exp_after_news_insertion.xml";

	@Autowired
	private NewsDAO newsDAO;

	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = {
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(AUTHOR_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(TAG_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(COMMENT_TABLE_DATA_XML_FILE_LOCATION)) };
		return new CompositeDataSet(dataSet);
	}

	@Test
	// This annotation works only when @Transactional is on DAO impl class.
	// If it is on test class then test goes ok, but it rolls back all the new data before we check the expected DB state..
	//@ExpectedDatabase(connection = "dbUnitDatabaseConnection", value = EXPECTED_AFTER_NEWS_INSERTION_DATASET_FILE_LOCATION, assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void insertNews() {
		News news = new News("Title4", "Short text4", "Full text4", LocalDateTime.of(2016, 04, 20, 02, 10, 30, 0), LocalDateTime.of(2016, 04, 20, 02, 10, 30, 0));
		Set<News> setOfNews = new HashSet<>();
		setOfNews.add(news);

		Author author1 = new Author(new Long(1), "Pushkin");
		Author author2 = new Author("some new author");
		Set<Author> authors = new HashSet<>();
		authors.add(author1);
		authors.add(author2);
		news.setAuthors(authors);
		author1.setNews(setOfNews);
		author2.setNews(setOfNews);

		Tag tag1 = new Tag("some new tag");
		Set<Tag> tags = new HashSet<>();
		tags.add(tag1);
		news.setTags(tags);
		tag1.setNews(setOfNews);

		Comment comment1 = new Comment("Text7", LocalDateTime.of(2016, 04, 20, 10, 10, 30, 0));
		Set<Comment> comments = new HashSet<>();
		comments.add(comment1);
		news.setComments(comments);
		comment1.setNews(news);

		long id = newsDAO.save(news);
		Assert.assertEquals(4L, id);
	}
/*
	@Test
	public void selectNewsById() {
		Long newsId = new Long(1);
		News newsExpected = new News(newsId, "Title1", "Short text1", "Full text1", LocalDateTime.of(2016, 04, 20, 12, 10, 30, 10), LocalDateTime.of(2016, 04, 20, 0, 0, 0, 0));

		Author author1 = new Author(new Long(1), "Pushkin");
		Author author2 = new Author(new Long(2), "Lermontov");
		Set<Author> authors = new HashSet<>();
		authors.add(author1);
		authors.add(author2);
		newsExpected.setAuthors(authors);

		Tag tag1 = new Tag("russian classical literature");
		Set<Tag> tags = new HashSet<>();
		tags.add(tag1);
		newsExpected.setTags(tags);

		Comment comment1 = new Comment(new Long(1), "Text1", LocalDateTime.of(116, 03, 20, 10, 10, 30, 12));
		Set<Comment> comments = new HashSet<>();
		newsExpected.setComments(comments);

		News newsRecieved = newsDAO.retrieveById(newsId);
		Assert.assertEquals(newsExpected, newsRecieved);
	}

	@Test
	public void selectAllNews() {
		List<News> listOfNews = null;
		listOfNews = (List<News>) newsDAO.retrieveAll(0, 10);
		Assert.assertEquals(3, listOfNews.size());
	}

	@Test
	public void updateNews() {
		Long newsId = new Long(1);
		News newsToUpdate = new News(newsId, "Title1111", "Short text1", "Full text1",
				LocalDateTime.parse("2016-04-20 12:10:30.10"),
				LocalDateTime.of(116, 03, 20, 0, 0));
		newsDAO.update(newsToUpdate);
		News newsRecieved = newsDAO.retrieveById(newsId);
		Assert.assertEquals(newsToUpdate, newsRecieved);
	}*/
}
