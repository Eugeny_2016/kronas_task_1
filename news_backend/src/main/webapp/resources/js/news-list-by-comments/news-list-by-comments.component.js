// without pure $http but with own custom service
(function() {
	angular.module('newsListByComments').
	  component('newsListByComments', {
		// Note: The URL is relative to our `index.html` file
		templateUrl: "resources/js/news-list-by-comments/news-list-by-comments.template.html", 
	    controller: ['News', 'generalInf', '$routeParams', '$rootScope', '$window', 
	                 function NewsListByCommentsController(News, generalInf, $routeParams, $rootScope, $window) {
	    	
	    	var self = this;
	    	
	    	$rootScope.newsPerAuthorShow = false;
	    	$rootScope.canGoBack = true;
	    	$rootScope.listOfNews = true;
	    	$rootScope.newsDetail = false;
	    	
	    	self.startPosDefault = 0;
	    	self.currentPageNumber = 1;
	    	self.newsPrevious = false;
	    	self.newsRemain = false;
	    	self.futureNews = undefined;
	    	self.startPos = generalInf.getStartPos();
	    	self.offset = generalInf.getOffset();
	    	self.newsPerPage = self.offset;
	    	
	    	self.newsFromREST = [ ];   
	    	self.newsFromREST = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset});
	    	
	    	var nextStartPos = self.startPos + self.newsPerPage;

	    	self.futureNews = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: nextStartPos, offset: self.offset}, function() {
	    		if (self.futureNews.length > 0) {
		    		self.newsRemain = true;
		    	}
	    	})
	    	
	    	self.previousNews = function() {
    			self.currentPageNumber = self.currentPageNumber - 1;
    			
    			self.startPos = self.startPos - parseInt(self.newsPerPage);
    			   			
    			self.newsFromREST = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset});
    			
    			var previousStartPos = self.startPos - parseInt(self.newsPerPage);

    			if (previousStartPos > 0) {
	    			self.futureNews = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset}, 
	    												function() {
	    				if (self.futureNews.length > 0) {
	    		    		self.newsRemain = true;
	    		    	} else {
	    		    		self.newsRemain = false;
	    		    	}
	    			})
    			}
    			if (self.currentPageNumber !== 1) {
		    		self.newsPrevious = true;
		    	} else {
		    		self.newsPrevious = false;
		    	}
		    	
    			self.newsRemain = true;
	    	}
	    	
    		self.nextNews = function() {
    			self.currentPageNumber = self.currentPageNumber + 1;
    			self.startPos = self.startPos + parseInt(self.newsPerPage);
    			
    			self.newsFromREST = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset});
    			
    			self.futureNews = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset}, 
    												function() {
    				if (self.futureNews.length !== 0) {
    		    		self.newsRemain = true;
    		    	} else {
    		    		self.newsRemain = false;
    		    	}
    			})
    			if (self.currentPageNumber > 1) {
		    		self.newsPrevious = true;
		    	}
	    	}
    		
    		
    		self.setNewsPerPage = function() {
    			// Here we use query() again cause we expect an array from our rest-service. 
    			// We just simply override the parameters, cause setter doesn't work as we want it to.
    			
    			// We use a callback function of the $resource action "query" cause we need to wait until the rest service returns future news.
    			
    			self.currentPageNumber = 1;
    			self.newsPrevious = false;
    			
    			self.startPos = self.startPosDefault;
    			self.offset = parseInt(self.newsPerPage);
    			
    			self.newsFromREST = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset});

    			self.futureNews = News.newsSeveral.query({topCommentedNewsAmount: $routeParams.topCommentedNewsAmount, startPos: self.startPos, offset: self.offset}, 
    												function() {
    				if (self.futureNews.length > 0) {
    		    		self.newsRemain = true;
    		    	} else {
    		    		self.newsRemain = false;
    		    	}
    			})
    		}
	    }
	  ]
	  });
	
}) ()
