// without pure $http but with own custom service
(function() {
	angular.module('newsCreation').
	  component('newsCreation', {
	    templateUrl: 'resources/js/news-creation/news-creation.template.html',
	    controller: ['News', 'Tag', 'Author', '$rootScope', '$window', 
	      function NewsDetailController(News, Tag, Author, $rootScope, $window) {
	    	var self = this;
	    	
	    	$rootScope.canGoBack = true;
	    	self.singleNews = {
	    		title: '', 
	    		shortText: '', 
	    		fullText: '', 
	    		authors: [] ,
	    		tags: []
	    	}
	    	
	    	self.tags = Tag.tagsAll.query(function() {
	    		self.selectedTagName = self.tags[0].tagName;
	    	});
	    	self.tagNamesEmbeded = [ ];
	    	
	    	self.authors = Author.authorsAll.query(function() {
	    		self.selectedAuthorName = self.authors[0].authorName;
	    	});
	    	self.authorNamesEmbeded = [ ];
	    	
	    	self.saveNews = function() {
	    		if (self.singleNews.title.length === 0) {
	    			$window.alert("Title must be filled in!");
	    			return;
	    		} else if (self.singleNews.title.length < 3 || self.singleNews.title.length > 30) {
	    			$window.alert("Title must be in between 3-30 characters!");
	    			return;
	    		} else if (self.singleNews.shortText.length === 0) {
	    			$window.alert("Short text must be filled in!");
	    			return;
	    		} else if (self.singleNews.shortText.length < 10 || self.singleNews.title.length > 100) {
	    			$window.alert("Short text must be in between 10-100 characters!");
	    			return;
	    		} else if (self.singleNews.fullText.length === 0) {
	    			$window.alert("Full text must be filled in!");
	    			return;
	    		} else if (self.singleNews.fullText.length < 10 || self.singleNews.title.length > 2000) {
	    			$window.alert("Full text must be in between 10-2000 characters!");
	    			return;
	    		} else if (self.singleNews.tags.length === 0) {
	    			$window.alert("Please add at least one tag to the news.");
	    			return;
	    		} else if (self.singleNews.authors.length === 0) {
	    			$window.alert("Please add at least one author to the news.");
	    			return;
	    		}    		
	    		News.newsCreate.save(self.singleNews, function() {
	    			$window.alert("Your news was successfully saved.");
	    			
	    			// очищаем все поля новости и выбранные теги на экране
		    		self.singleNews = {
	    	    		title: '', 
	    	    		shortText: '', 
	    	    		fullText: '', 
	    	    		authors: [] ,
	    	    		tags: []
	    	    	}
		    		self.tagNamesEmbeded = [ ];
			    	self.authorNamesEmbeded = [ ];
	    		});
	    	};
	    	
	    	self.addTagToNews = function() {
	    		var i, len, tagToRemove;
	    		len = self.tags.length;
	    		
	    		for (i = 0; i < len; i++) {
	    			var currentTagName = self.tags[i].tagName;
	    			if (!currentTagName.localeCompare(self.selectedTagName)) {
	    				tagToRemove = i;
	    				break;
	    			}
	    		}
	    		self.singleNews.tags.push(self.tags[tagToRemove]);
	    		self.tags.splice(tagToRemove, 1);
	    		self.tagNamesEmbeded.push(self.selectedTagName);
	    	}
	    	
	    	self.addAuthorToNews = function() {
	    		var i, len, authorToRemove;
	    		len = self.authors.length;
	    		
	    		for (i = 0; i < len; i++) {
	    			var currentAuthorName = self.authors[i].authorName;
	    			if (!currentAuthorName.localeCompare(self.selectedAuthorName)) {
	    				authorToRemove = i;
	    				break;
	    			}
	    		}
	    		self.singleNews.authors.push(self.authors[authorToRemove]);
	    		self.authors.splice(authorToRemove, 1);
	    		self.authorNamesEmbeded.push(self.selectedAuthorName);
	    	}
	      }
	    ]
	  });
	
}) ()