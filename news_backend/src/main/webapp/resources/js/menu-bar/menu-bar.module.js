(function() {
	angular.module('menuBar', ['core']).
	
	controller('MenuBarController', ['$rootScope', 'News', 'Tag', 'Author', '$window', function($rootScope, News, Tag, Author, $window) {
		var self = this;
		//this.gi = generalInf;
		//this.query = generalInf.getQuery();
		//generalInf.setQuery(giq);
		
		$rootScope.listOfNews = true;
		$rootScope.newsDetail = false;
		
		$rootScope.newsPerTagShow = false;
		$rootScope.newsPerAuthorShow = false;
		$rootScope.newsTopCommentsShow = false;
		
		self.showTagsWithAmountOfNews = function(tagName) {
			$rootScope.newsPerTagShow = true;
			self.tagsTO = Tag.tagsWithAmountOfNews.query();
		}
		
		self.showAuthorsWithAmountOfNews = function(tagName) {
			$rootScope.newsPerAuthorShow = true;
			self.authorsTO = Author.authorsWithAmountOfNews.query();
		}
		
		self.showCommentTops = function(top) {
			$rootScope.newsTopCommentsShow = true;
		}
		
		self.deleteNews = function(newsId) {
    		News.newsDelete.deleteNews({newsId: newsId}, function() {
    			$window.alert('News was successfully deleted.')
    		});    		
    	}
		
	}]);
	
}) ();