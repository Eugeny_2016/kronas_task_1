// without pure $http but with own custom service
(function() {
	angular.module('newsList').
	  component('newsList', {
		// Note: The URL is relative to our `index.html` file
		templateUrl: "resources/js/news-list/news-list.template.html", 
	    controller: ['News', 'generalInf', '$rootScope', '$window', function NewsListController(News, generalInf, $rootScope, $window) {
	    	
	    	//Since we are making the assignment of the newsFromFile property in a callback function, 
	    	//where the this value is not defined, we also introduce a local variable called selfController  
	    	//that points back to the controller instance.
	    	var self = this;
	    	
	    	$rootScope.canGoBack = false;
	    	$rootScope.listOfNews = true;
	    	$rootScope.newsDetail = false;
	    	
	    	self.startPosDefault = 0;
	    	self.currentPageNumber = 1;
	    	self.newsPrevious = false;
	    	self.newsRemain = false;
	    	self.futureNews = undefined;
	    	self.startPos = generalInf.getStartPos();
	    	self.offset = generalInf.getOffset();
	    	self.newsPerPage = self.offset;
	    	self.noError = true;
	    	self.errorInfo = {};
	    	
	    	self.newsFromREST = [ ];	      
	    	self.newsFromREST = News.newsSeveral.query();
	    	
	    	var nextStartPos = self.startPos + self.newsPerPage;

	    	self.futureNews = News.newsSeveral.query({startPos: nextStartPos, offset: self.offset}, function() {
	    		if (self.futureNews.length > 0) {
		    		self.newsRemain = true;
		    	}
	    	}, function(error) {
	    		self.noError = false;	// нужно разобраться, как работать с пришедшей ошибкой
	    		self.errorInfo = error;
	    		//self.errorInfo = {errorMessage: error.message, errorStatus: error.httpStatus};
	    		alert(message);
	    	})
	    	
	    	self.deleteNews = function(newsId) {
	    		News.newsDelete.deleteNews({newsId: newsId}, function() {
	    			var i;
	    			var length = self.newsFromREST.length;
	    			for (i = 0; i < length; i++) {
	    				var currentNewsId = self.newsFromREST[i].newsId;
	    				if (currentNewsId === newsId) {
	    					self.newsFromREST.splice(i, 1);
	    					break;
		    			}
	    			}
	    			$window.alert('News was successfully deleted.');
	    		});    		
	    	}
	    	
	    	self.previousNews = function() {
    			self.currentPageNumber = self.currentPageNumber - 1;
    			
    			self.startPos = self.startPos - parseInt(self.newsPerPage);
    			   			
    			self.newsFromREST = News.newsSeveral.query({startPos: self.startPos, offset: self.offset});
    			
    			var previousStartPos = self.startPos - parseInt(self.newsPerPage);

    			if (previousStartPos > 0) {
	    			self.futureNews = News.newsSeveral.query({startPos: previousStartPos, offset: self.offset}, 
	    												function() {
	    				if (self.futureNews.length > 0) {
	    		    		self.newsRemain = true;
	    		    	} else {
	    		    		self.newsRemain = false;
	    		    	}
	    			})
    			}
    			if (self.currentPageNumber !== 1) {
		    		self.newsPrevious = true;
		    	} else {
		    		self.newsPrevious = false;
		    	}
		    	
    			self.newsRemain = true;
	    	}
	    	
    		self.nextNews = function() {
    			self.currentPageNumber = self.currentPageNumber + 1;
    			self.startPos = self.startPos + parseInt(self.newsPerPage);
    			
    			self.newsFromREST = News.newsSeveral.query({startPos: self.startPos, offset: self.offset});
    			
    			var nextStartPos = self.startPos + parseInt(self.newsPerPage);
    			
    			self.futureNews = News.newsSeveral.query({startPos: nextStartPos, offset: self.offset}, 
    												function() {
    				if (self.futureNews.length !== 0) {
    		    		self.newsRemain = true;
    		    	} else {
    		    		self.newsRemain = false;
    		    	}
    			})
    			if (self.currentPageNumber > 1) {
		    		self.newsPrevious = true;
		    	}
	    	}
    		
    		
    		self.setNewsPerPage = function() {
    			// Here we use query() again cause we expect an array from our rest-service. 
    			// We just simply override the parameters, cause setter doesn't work as we want it to.
    			
    			// We use a callback function of the $resource action "query" cause we need to wait until the rest service returns future news.
    			
    			self.currentPageNumber = 1;
    			self.newsPrevious = false;
    			
    			self.startPos = self.startPosDefault;
    			self.offset = parseInt(self.newsPerPage);
    			
    			self.newsFromREST = News.newsSeveral.query({startPos: self.startPos, offset: self.offset});
    			
    			var nextStartPos = self.startPos + parseInt(self.newsPerPage);
    			
    			self.futureNews = News.newsSeveral.query({startPos: nextStartPos, offset: self.offset}, 
    												function() {
    				if (self.futureNews.length > 0) {
    		    		self.newsRemain = true;
    		    	} else {
    		    		self.newsRemain = false;
    		    	}
    			})
    		}
	    }
	  ]
	  });
	
}) ()
