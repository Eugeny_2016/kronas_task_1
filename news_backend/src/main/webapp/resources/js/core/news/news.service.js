(function() {
	angular.module('core.news').
	  factory('News', ['$resource', 'generalInf', function($resource, generalInf) {
		  
		  // Why do I use a dummy empty data arg in get methods ?
		  
		  // The Content-Type header gets removed if the request does not contain any data (a request body).
		  // I think this is the expected behaviour since GET requests do not have a body.
		  // A POST method in the other hand, will set the content-type as you expect, as long it has data in the request body.
		  
		  return {
			newsCreate: $resource(generalInf.getUrlNewsAllString(), {}, {
				save: {method: 'POST', headers: {'Content-Type': 'application/json'}}
			}),
			newsById: $resource(generalInf.getUrlNewsByIdString(), {newsId:'@id'}, {
				query: {data: '', method: 'GET', isArray: false, headers: {'Content-Type': 'application/json'}}
	        }),
	        newsSeveral: $resource(generalInf.getUrlNewsAllString(), {}, { 
	        	query: {data: '', method: 'GET', params: {startPos: generalInf.getStartPos(), offset: generalInf.getOffset()}, isArray: true, headers: {'Content-Type': 'application/json'}}
	        }), 
	        newsUpdate: $resource(generalInf.getUrlNewsByIdString(), {}, {
				update: {method: 'PUT', headers: {'Content-Type': 'application/json'}}
			}),
			newsDelete: $resource(generalInf.getUrlNewsByIdString(), {}, {
				deleteNews: {data: '', method: 'DELETE', headers: {'Content-Type': 'application/json'}}
			}), 
			commentsByNewsId: $resource(generalInf.getUrlCommentsByNewsId(), {}, {
				query: {data: '', method: 'GET', isArray: true, headers: {'Content-Type': 'application/json'}}
			})
		  }
		}
	    
	]);
}) ()