(function() {
	angular.module('core.comment').
	  factory('Comment', ['$resource', '$routeParams', 'generalInf', function($resource, $routeParams, generalInf) {
		  
		  // $resource("../rest/api"}).get(); return an object.
		  // $resource("../rest/api").query(); return an array.
		  
		  return {
			commentAddToNews: $resource(generalInf.getUrlCommentsByNewsId(), {}, {
				save: {method: 'POST', headers: {'Content-Type': 'application/json'}}
			}), 
			commentsByNewsId: $resource(generalInf.getUrlCommentsByNewsId(), {}, {
				query: {data: '', method: 'GET', isArray: true, headers: {'Content-Type': 'application/json'}}
			})
		}
	  }
	    
	]);
}) ()