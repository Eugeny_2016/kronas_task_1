(function() {
	angular.module('core').
	  service('generalInf', function() {
		
		 var urlBaseString = 'http://localhost:8081/news_backend/';
		 var urlNewsAllString = 'news/';
		 var urlNewsByIdString = 'news/:newsId';
		 var urlCommentsByNewsId = 'news/:newsId/comments/';
		 var urlCommentByIdString = 'news/:newsId/comments/:commentId';
		 var urlAuthorString = 'authors/';
		 var urlAuthorByIdString = 'authors/:authorId';
		 var urlTagString = 'tags/';
		 var urlTagByNameString = 'tags/:tagName';	
		 
		// default values of query and orderProp
		 var query = '';
		 var orderProp = '-creationDate';
		 
		// default values of startPos and offset
		 var startPos = 0;
		 var offset = 10;
		 
		 return {
	            getUrlBaseString: function () {
	                return urlBaseString;
	            },
	            setUrlBaseString: function(value) {
	            	urlBaseString = value;
	            }, 
	            getUrlNewsAllString: function () {
	                return (urlBaseString + urlNewsAllString);
	            },
	            setUrlNewsAllString: function(value) {
	            	urlNewsAllString = value;
	            }, 
	            getUrlNewsByIdString: function () {
	                return (urlBaseString + urlNewsByIdString);
	            },
	            setUrlNewsByIdString: function(value) {
	            	urlNewsByIdString = value;
	            },
	            getUrlCommentsByNewsId: function () {
	                return (urlBaseString + urlCommentsByNewsId);
	            },
	            setUrlCommentsByNewsId: function(value) {
	            	urlCommentsByNewsId = value;
	            }, 
	            getUrlCommentByIdString: function () {
	                return (urlBaseString + urlCommentByIdString);
	            },
	            setUrlCommentByIdString: function(value) {
	            	urlCommentByIdString = value;
	            },
	            getUrlAuthorString: function () {
	                return (urlBaseString + urlAuthorString);
	            },
	            setUrlAuthorString: function(value) {
	            	urlAuthorString = value;
	            },
	            getUrlAuthorByIdString: function () {
	                return (urlBaseString + urlAuthorByIdString);
	            },
	            setUrlAuthorByIdString: function(value) {
	            	urlAuthorByIdString = value;
	            },
	            getUrlTagString: function () {
	                return (urlBaseString + urlTagString);
	            },
	            setUrlTagString: function(value) {
	            	urlTagString = value;
	            },
	            getUrlTagByNameString: function () {
	                return (urlBaseString + urlTagByNameString);
	            },
	            setUrlTagByNameString: function(value) {
	            	urlTagByNameString = value;
	            },
	            getUrlCommentString: function () {
	                return (urlBaseString + urlCommentString);
	            },
	            setUrlCommentString: function(value) {
	            	urlCommentString = value;
	            }, 
	            getQuery: function () {
	                return query;
	            },
	            setQuery: function(value) {
	            	query = value;
	            }, 
	            getOrderProp: function () {
	                return orderProp;
	            },
	            setOrderProp: function(value) {
	            	orderProp = value;
	            }, 
	            getStartPos: function () {
	                return startPos;
	            },
	            setStartPos: function(value) {
	            	startPos = value;
	            }, 
	            getOffset: function () {
	                return offset;
	            },
	            setOffset: function(value) {
	            	offset = value;
	            }
	        };
		
	});

}) ()