(function() {
	angular.module('core.author').
	  factory('Author', ['$resource', '$routeParams', 'generalInf', function($resource, $routeParams, generalInf) {
		  
		  // $resource("../rest/api"}).get(); return an object.
		  // $resource("../rest/api").query(); return an array.
		  
		  return {
			  authorCreate: $resource(generalInf.getUrlAuthorString(), {}, {
				  save: {method: 'POST', headers: {'Content-Type': 'application/json'}}
			  }),
			  authorsAll: $resource(generalInf.getUrlAuthorString(), {}, {
				  query: {data: '', method: 'GET', isArray: true, headers: {'Content-Type': 'application/json'}}
			  }), 
			  authorsWithAmountOfNews: $resource(generalInf.getUrlAuthorString(), {}, {
				  query: {data: '', method: 'GET', params: {isOnlyAmountOfNews: true}, isArray: true, headers: {'Content-Type': 'application/json'}}
			  }), 
			  authorUpdate: $resource(generalInf.getUrlAuthorByIdString(), {}, {
				  update: {method: 'PUT', headers: {'Content-Type': 'application/json'}}
			  }),
			  authorDelete: $resource(generalInf.getUrlAuthorByIdString(), {}, {
				  deleteAuthor: {data: '', method: 'DELETE', headers: {'Content-Type': 'application/json'}}
			  })
		}
	  }
	    
	]);
}) ()