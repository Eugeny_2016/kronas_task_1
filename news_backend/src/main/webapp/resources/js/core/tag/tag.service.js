(function() {
	angular.module('core.tag').
	  factory('Tag', ['$resource', '$routeParams', 'generalInf', function($resource, $routeParams, generalInf) {
		  
		  // $resource("../rest/api"}).get(); return an object.
		  // $resource("../rest/api").query(); return an array.
		  
		  return {
			  tagCreate: $resource(generalInf.getUrlTagString(), {}, {
				  save: {method: 'POST', headers: {'Content-Type': 'application/json'}}
			  }),
			  tagsAll: $resource(generalInf.getUrlTagString(), {}, {
				  query: {data: '', method: 'GET', isArray: true, headers: {'Content-Type': 'application/json'}}
			  }), 
			  tagsWithAmountOfNews: $resource(generalInf.getUrlTagString(), {}, {
				  query: {data: '', method: 'GET', params: {isOnlyAmountOfNews: true}, isArray: true, headers: {'Content-Type': 'application/json'}}
			  }), 
			  tagUpdate: $resource(generalInf.getUrlTagByNameString(), {}, {
				  update: {method: 'PUT', headers: {'Content-Type': 'application/json'}}
			  }),
			  tagDelete: $resource(generalInf.getUrlTagByNameString(), {}, {
				  deleteTag: {data: '', method: 'DELETE', headers: {'Content-Type': 'application/json'}}
			  })
		  }
	  }
	    
	]);
}) ()