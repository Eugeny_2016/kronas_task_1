(function() {
	// 'ngRoute' is used here as a dependent module, because newsDetail uses $routeParams object
	// though since ngRoute is also a dependency of the main clientApp module, its services and directives 
	// are already available everywhere in the application (including the phoneDetail component).
	// This means that our application would continue to work even if we didn't include ngRoute in the list of 
	// dependencies for the phoneDetail component. Although it might be tempting to omit dependencies of a sub-module 
	// that are already imported by the main module, it breaks our hard-earned modularity.

	//Imagine what would happen if we decided to copy the phoneDetail feature over to another project that 
	// does not declare a dependency on ngRoute. The injector would not be able to provide $routeParams and 
	// our application would break.
	
	//The takeaway here is:
	// Always be explicit about the dependecies of a sub-module. 
	// Do not rely on dependencies inherited from a parent module 
	// (because that parent module might not be there some day).
	angular.module('newsDetail', ['ngRoute', 'core']);
	
}) ()