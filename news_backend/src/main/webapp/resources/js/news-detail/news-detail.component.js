// without pure $http but with own custom service

// for creating an alias for the components controller write like this: 
//.component('foo', {
//  controllerAs: 'fooCtrl',
//  template:

(function() {
	angular.module('newsDetail').
	  component('newsDetail', {
	    templateUrl: 'resources/js/news-detail/news-detail.template.html',
	    controller: ['News', 'Comment', 'generalInf', '$routeParams', '$window', '$rootScope', 
	      function NewsDetailController(News, Comment, generalInf, $routeParams, $window, $rootScope) {
	    	var self = this;
	    	
	    	$rootScope.canGoBack = true;
	    	$rootScope.listOfNews = false;
	    	$rootScope.newsDetail = true;
	    	
	    	self.startPosDefault = 0;
	    	self.currentPageNumber = 1;
	    	self.commentsPrevious = false;
	    	self.commentsRemain = false;
	    	self.futureComments = undefined;
	    	self.startPos = generalInf.getStartPos();
	    	self.offset = generalInf.getOffset();
	    	self.commentsPerPage = self.offset;
	    		    	
	    	self.singleNews = News.newsById.query({newsId: $routeParams.newsId}, function() {
	    		self.newComment = { 
	    			//newsId: self.singleNews.newsId, 
	    			commentText: '', 
    				creationDate: null
	    	    }
	    		$rootScope.currentNews = self.singleNews;
	    	});
	    	
	    	self.commentsToShow = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: self.startPos, offset: self.offset});
	    	
	    	var nextStartPos = self.startPos + self.commentsPerPage;

	    	self.futureComments = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: nextStartPos, offset: self.offset}, function() {
	    		if (self.futureComments.length > 0) {
		    		self.commentsRemain = true;
		    	}
	    	})
	    	
	    	self.previousComments = function() {
    			self.currentPageNumber = self.currentPageNumber - 1;
    			
    			self.startPos = self.startPos - parseInt(self.commentsPerPage);
    			   			
    			self.commentsToShow = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: self.startPos, offset: self.offset});
    			
    			var previousStartPos = self.startPos - parseInt(self.commentsPerPage);

    			if (previousStartPos > 0) {
	    			self.futureComments = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: previousStartPos, offset: self.offset}, 
	    												function() {
	    				if (self.futureComments.length > 0) {
	    		    		self.commentsRemain = true;
	    		    	} else {
	    		    		self.commentsRemain = false;
	    		    	}
	    			})
    			}
    			if (self.currentPageNumber !== 1) {
		    		self.commentsPrevious = true;
		    	} else {
		    		self.commentsPrevious = false;
		    	}
		    	
    			self.commentsRemain = true;
	    	}
	    	
    		self.nextComments = function() {
    			self.currentPageNumber = self.currentPageNumber + 1;
    			self.startPos = self.startPos + parseInt(self.commentsPerPage);

    			self.commentsToShow = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: self.startPos, offset: self.offset});
    			
    			var nextStartPos = self.startPos + parseInt(self.commentsPerPage);

    			self.futureComments = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: nextStartPos, offset: self.offset}, 
    												function() {
    				if (self.futureComments.length !== 0) {
    		    		self.commentsRemain = true;
    		    	} else {
    		    		self.commentsRemain = false;
    		    	}
    			})
    			if (self.currentPageNumber > 1) {
		    		self.commentsPrevious = true;
		    	}
	    	}
    		
    		
    		self.setCommentsPerPage = function() {
    			// Here we use query() again cause we expect an array from our rest-service. 
    			// We just simply override the parameters, cause setter doesn't work as we want it to.
    			
    			// We use a callback function of the $resource action "query" cause we need to wait until the rest service returns future comments.
    			
    			self.currentPageNumber = 1;
    			self.commentsPrevious = false;
    			
    			self.startPos = self.startPosDefault;
    			self.offset = parseInt(self.commentsPerPage);
    			
    			self.commentsToShow = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: self.startPos, offset: self.offset});
    			
    			var nextStartPos = self.startPos + parseInt(self.commentsPerPage);
    			
    			self.futureComments = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: nextStartPos, offset: self.offset}, 
    												function() {
    				if (self.futureComments.length > 0) {
    		    		self.commentsRemain = true;
    		    	} else {
    		    		self.commentsRemain = false;
    		    	}
    			})
    		}
    		
    		self.addComment = function() {
    			Comment.commentAddToNews.save({newsId: $routeParams.newsId}, self.newComment, function() {
    				window.alert('Comment was successfully added to the news.');
    				self.commentsToShow = Comment.commentsByNewsId.query({newsId: $routeParams.newsId, startPos: self.startPos, offset: self.offset})
    				self.newComment.commentText = null;
    			})
    		}
	      }
	    ]
	  });
	
}) ()