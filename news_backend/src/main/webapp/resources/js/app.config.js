(function() {
	angular.module('clientApp').
	  config(['$locationProvider', '$resourceProvider', '$routeProvider',
	    function config($locationProvider, $resourceProvider, $routeProvider) {
	      $locationProvider.hashPrefix('!');
	      
	      // By default, trailing slashes will be stripped from the calculated URLs, which can pose problems with server backends 
	      // that do not expect that behavior. This can be disabled by configuring the $resourceProvider like
	      $resourceProvider.defaults.stripTrailingSlashes = false;
	      
	      // I can't use /news url for two and more operation, cause one will override another
	      // don't know how to solve this issue...
	      $routeProvider.
	      	when('/news', {
	          template: '<news-list></news-list>'
	        }).
	        when('/newsByTag', {
	          params: {
        		  tagName: '*'
              },
	          template: '<news-list-by-tag></news-list-by-tag>'
		    }).
		    when('/newsByAuthor', {
	          params: {
        		  authorId: '*'
              },
	          template: '<news-list-by-author></news-list-by-author>'
		    }).
		    when('/newsByTopComments', {
	          params: {
	        	  topCommentedNewsAmount: '*'
              },
	          template: '<news-list-by-comments></news-list-by-comments>'
		    }).
	        when('/news/creation', {
		      template: '<news-creation></news-creation>'
		    }).
		    when('/news/edit/:newsId', {
		      template: '<news-edit></news-edit>'
		    }).
	        when('/news/:newsId', {
	          template: '<news-detail></news-detail>'
	        }).
	        when('/tags/', {
	          template: '<tags></tags>'
	        }).
	        when('/authors/', {
	          template: '<authors></authors>'
	        }).
	        otherwise('/news');
	    }
	  ]);
	
}) ()
