// without pure $http but with own custom service

// for creating an alias for the components controller write like this: 
//.component('foo', {
//  controllerAs: 'fooCtrl',
//  template:

(function() {
	angular.module('authors').
	  component('authors', {
	    templateUrl: 'resources/js/authors/authors.template.html',
	    controller: ['Author', 'generalInf', '$document', '$routeParams', '$window', '$rootScope', 
	      function AuthorsController(Author, generalInf, $document, $routeParams, $window, $rootScope) {
	    	var self = this;
	    	
	    	$rootScope.canGoBack = true;
	    	$rootScope.listOfNews = true;
	    		
	    	self.edit = false;
	    	
	    	self.authors = Author.authorsAll.query(function() {
	    		self.selectedAuthorNameToChange = self.authors[0].authorName;
	    	});
	    	
	    	self.updateAuthor = function() {
	    		var i, currentAuthorName;
	    		for(i = 0; i < self.authors.length; i++) {
	    			currentAuthorName = self.authors[i].authorName;
	    			if (!currentAuthorName.localeCompare(self.oldValueSelected)) {
	    				self.oldAuthorId = self.authors[i].authorId;
	    				break;
	    			}
	    		}
	    		
	    		self.Author = {
	    			authorId: self.oldAuthorId, 
	    			authorName: self.selectedAuthorNameToChange
	    		}
	    		
	    		Author.authorUpdate.update({authorId: self.oldAuthorId}, self.Author, function() {
	    			self.authors = Author.authorsAll.query(function() {
			    		self.selectedAuthorNameToChange = self.authors[0].authorName;
			    		$window.alert('Author has been successfully updated.');
			    	});
	    		});
	    		self.edit = false;
	    	}
	    	
	    	self.deleteAuthor = function() {
	    		var i;
	    		for(i = 0; i < self.authors.length; i++) {
	    			var currentAuthorName = self.authors[i].authorName;
	    			if (!currentAuthorName.localeCompare(self.oldValueSelected)) {
	    				self.oldAuthorId = self.authors[i].authorId;
	    				break;
	    			}
	    		}
	    		
	    		Author.authorDelete.deleteAuthor({authorId: self.oldAuthorId}, function() {
	    			self.authors = Author.authorsAll.query(function() {
			    		self.selectedAuthorNameToChange = self.authors[0].authorName;
	    				$window.alert('Author has been successfully deleted.');
	    			});
	    		});
	    	}
	    	
	    	self.addAuthor = function() {
	    		self.newAuthor = {
		    			authorName: self.newAuthorName
		    		}
		    		
		    		Author.authorCreate.save(self.newAuthor, function() {	    			
		    			self.authors = Author.authorsAll.query(function() {
				    		self.selectedAuthorNameToChange = self.authors[0].authorName;
		    				self.newAuthorName = null;
		    				$window.alert('Author has been successfully added.');
		    			});
		    		});
	    	}
	    	
	    	self.editAuthor = function() {
	    		self.oldValueSelected = self.selectedAuthorNameToChange;
	    	}
	    	
	      }
	    ]
	  });
	
}) ()