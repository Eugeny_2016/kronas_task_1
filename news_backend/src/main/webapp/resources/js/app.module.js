(function() {
	angular.module('clientApp', ['ngRoute', 'core', 'header', 'menuBar', 'newsList', 'newsListByTag', 'tags', 'authors', 
	                             'newsListByAuthor', 'newsListByComments', 'newsDetail', 'newsCreation', 'newsEdit']);

}) ()
