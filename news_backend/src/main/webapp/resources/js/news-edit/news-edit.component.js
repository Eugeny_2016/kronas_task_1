// without pure $http but with own custom service
(function() {
	angular.module('newsEdit').
	  component('newsEdit', {
	    templateUrl: 'resources/js/news-edit/news-edit.template.html',
	    controller: ['News', 'Tag', 'Author', '$routeParams', '$rootScope', '$window', 
	      function NewsEditController(News, Tag, Author, $routeParams, $rootScope, $window) {
	    	var self = this;
	    	var i, j, lenOfAddedTags, lenOfAllTags, lenOfAddedAuthors, lenOfAllAuthors;
	    	
	    	$rootScope.canGoBack = true;
	    	self.isErrorHappened = false;
	    	
	    	self.singleNews = News.newsById.query({newsId: $routeParams.newsId}, function() {
	    		lenOfAddedTags = self.singleNews.tags.length;
	    		lenOfAddedAuthors = self.singleNews.authors.length;
	    		
	    		self.selectedTagNameToDelete = self.singleNews.tags[0].tagName;
	    		self.selectedAuthorNameToDelete = self.singleNews.authors[0].authorName;
	    	});
	    	
	    	self.tags = Tag.tagsAll.query(function() {
	    		lenOfAllTags = self.tags.length;
	    		
	    		for (i = 0; i < lenOfAddedTags; i++) {
		    		for (j = 0; j < lenOfAllTags; j++) {
		    			var currentTagName = self.tags[j].tagName;
		    			if (!currentTagName.localeCompare(self.singleNews.tags[i].tagName)) {
		    				self.tags.splice(j, 1);
		    				lenOfAllTags--;
		    			}
		    		}
		    	}    		
	    		self.selectedTagNameToAdd = self.tags[0].tagName;
	    	});
	    	
	    	self.authors = Author.authorsAll.query(function() {
	    		lenOfAllAuthors = self.authors.length;
	    		
	    		for (i = 0; i < lenOfAddedAuthors; i++) {
		    		for (j = 0; j < lenOfAllAuthors; j++) {
		    			var currentAuthorName = self.authors[j].authorName;
		    			if (!currentAuthorName.localeCompare(self.singleNews.authors[i].authorName)) {
		    				self.authors.splice(j, 1);
		    				lenOfAllAuthors--;
		    			}
		    		}
		    	}    		
	    		self.selectedAuthorNameToAdd = self.authors[0].authorName;
	    	});

	    	self.saveNews = function() {
	    		if (self.singleNews.title.length === 0) {
	    			$window.alert("Title must be filled in!");
	    		} else if (self.singleNews.title.length < 3 || self.singleNews.title.length > 30) {
	    			$window.alert("Title must be in between 3-30 characters!");
	    		} else if (self.singleNews.shortText.length === 0) {
	    			$window.alert("Short text must be filled in!");
	    		} else if (self.singleNews.shortText.length < 10 || self.singleNews.title.length > 100) {
	    			$window.alert("Short text must be in between 10-100 characters!");
	    		} else if (self.singleNews.fullText.length === 0) {
	    			$window.alert("Full text must be filled in!");
	    		} else if (self.singleNews.fullText.length < 10 || self.singleNews.title.length > 2000) {
	    			$window.alert("Full text must be in between 10-2000 characters!");
	    		} else if (self.singleNews.tags.length === 0) {
	    			$window.alert("Please add at least one tag to the news.");
	    		} else if (self.singleNews.authors.length === 0) {
	    			$window.alert("Please add at least one author to the news.");
	    		}
	    		self.singleNews.creationDate = null;
	    		
	    		News.newsUpdate.update({newsId: self.singleNews.newsId}, self.singleNews, 
    				function() {
	    				self.isErrorHappened = false;
		    			$window.alert("Your news was successfully updated.");
		    		}, 
		    		function(error) {
		    			self.isErrorHappened = true;
		    			self.error = error;
		    			self.singleNews = News.newsById.query({newsId: $routeParams.newsId}, function() {
		    	    		lenOfAddedTags = self.singleNews.tags.length;
		    	    		lenOfAddedAuthors = self.singleNews.authors.length;
		    	    		
		    	    		self.selectedTagNameToDelete = self.singleNews.tags[0].tagName;
		    	    		self.selectedAuthorNameToDelete = self.singleNews.authors[0].authorName;
		    	    	});
		    			
		    			self.tags = Tag.tagsAll.query(function() {
		    	    		lenOfAllTags = self.tags.length;
		    	    		
		    	    		for (i = 0; i < lenOfAddedTags; i++) {
		    		    		for (j = 0; j < lenOfAllTags; j++) {
		    		    			var currentTagName = self.tags[j].tagName;
		    		    			if (!currentTagName.localeCompare(self.singleNews.tags[i].tagName)) {
		    		    				self.tags.splice(j, 1);
		    		    				lenOfAllTags--;
		    		    			}
		    		    		}
		    		    	}    		
		    	    		self.selectedTagNameToAdd = self.tags[0].tagName;
		    	    	});
		    	    	
		    	    	self.authors = Author.authorsAll.query(function() {
		    	    		lenOfAllAuthors = self.authors.length;
		    	    		
		    	    		for (i = 0; i < lenOfAddedAuthors; i++) {
		    		    		for (j = 0; j < lenOfAllAuthors; j++) {
		    		    			var currentAuthorName = self.authors[j].authorName;
		    		    			if (!currentAuthorName.localeCompare(self.singleNews.authors[i].authorName)) {
		    		    				self.authors.splice(j, 1);
		    		    				lenOfAllAuthors--;
		    		    			}
		    		    		}
		    		    	}    		
		    	    		self.selectedAuthorNameToAdd = self.authors[0].authorName;
		    	    	});
		    		});
	    	};
	    	
	    	self.addTagToNews = function() {
	    		var i, len, tagToRemove;
	    		len = self.tags.length;
	    		
	    		for (i = 0; i < len; i++) {
	    			var currentTagName = self.tags[i].tagName;
	    			if (!currentTagName.localeCompare(self.selectedTagNameToAdd)) {
	    				tagToRemove = i;
	    				break;
	    			}
	    		}
	    		self.singleNews.tags.push(self.tags[tagToRemove]);
	    		self.tags.splice(tagToRemove, 1);
	    		self.selectedTagNameToAdd = self.tags[0].tagName;
	    	}
	    	
	    	self.addAuthorToNews = function() {
	    		var i, len, authorToRemove;
	    		len = self.authors.length;
	    		
	    		for (i = 0; i < len; i++) {
	    			var currentAuthorName = self.authors[i].authorName;
	    			if (!currentAuthorName.localeCompare(self.selectedAuthorNameToAdd)) {
	    				authorToRemove = i;
	    				break;
	    			}
	    		}
	    		self.singleNews.authors.push(self.authors[authorToRemove]);
	    		self.authors.splice(authorToRemove, 1);
	    		self.selectedAuthorNameToAdd = self.authors[0].authorName;
	    	}
	    	
	    	self.deleteTagFromNews = function() {
	    		var i, len, tagToRemove;
	    		len = self.singleNews.tags.length;
	    		
	    		for (i = 0; i < len; i++) {
	    			var currentTagName = self.singleNews.tags[i].tagName;
	    			if (!currentTagName.localeCompare(self.selectedTagNameToDelete)) {
	    				tagToRemove = i;
	    			}
	    		}
	    		if (self.singleNews.tags.length === 1) {
	    			$window.alert("There must be at least one tag left.");
	    		} else {
	    			self.singleNews.tags.splice(tagToRemove, 1);
	    			self.selectedTagNameToDelete = self.singleNews.tags[0].tagName;
	    		}
	    	}
	    	
	    	self.deleteAuthorFromNews = function() {
	    		var i, len, authorToRemove;
	    		len = self.singleNews.authors.length;
	    		
	    		for (i = 0; i < len; i++) {
	    			var currentAuthorName = self.singleNews.authors[i].authorName;
	    			if (!currentAuthorName.localeCompare(self.selectedAuthorNameToDelete)) {
	    				authorToRemove = i;
	    			}
	    		}
	    		if (self.singleNews.authors.length === 1) {
	    			$window.alert("There must be at least one author left.");
	    		} else {
	    			self.singleNews.authors.splice(authorToRemove, 1);
	    			self.selectedAuthorNameToDelete = self.singleNews.authors[0].authorName;
	    		}
	    	}
	      }
	    ]
	  });
	
}) ()