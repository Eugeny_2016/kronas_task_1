// without pure $http but with own custom service

// for creating an alias for the components controller write like this: 
//.component('foo', {
//  controllerAs: 'fooCtrl',
//  template:

(function() {
	angular.module('tags').
	  component('tags', {
	    templateUrl: 'resources/js/tags/tags.template.html',
	    controller: ['Tag', 'generalInf', '$document', '$routeParams', '$window', '$rootScope', 
	      function TagsController(Tag, generalInf, $document, $routeParams, $window, $rootScope) {
	    	var self = this;
	    	
	    	$rootScope.canGoBack = true;
	    	$rootScope.listOfNews = true;
	    		
	    	self.edit = false;
	    	
	    	self.tags = Tag.tagsAll.query(function() {
	    		self.selectedTagNameToChange = self.tags[0].tagName;
	    	});
	    	
	    	self.updateTag = function() {
	    		self.tag = {
	    			tagName: self.selectedTagNameToChange
	    		}

	    		Tag.tagUpdate.update({tagName: self.oldValueSelected}, self.tag, function() {
	    			self.tags = Tag.tagsAll.query(function() {
			    		self.selectedTagNameToChange = self.tags[0].tagName;
			    		$window.alert('Tag has been successfully updated.');
			    	});
	    		});
	    		self.edit = false;
	    	}
	    	
	    	self.deleteTag = function() {
	    		Tag.tagDelete.deleteTag({tagName: self.selectedTagNameToChange}, function() {
	    			self.tags = Tag.tagsAll.query(function() {
			    		self.selectedTagNameToChange = self.tags[0].tagName;
	    				$window.alert('Tag has been successfully deleted.');
	    			});
	    		});
	    	}
	    	
	    	self.addTag = function() {
	    		self.newTag = {
	    			tagName: self.newTagName
	    		}
	    		
	    		Tag.tagCreate.save(self.newTag, function() {	    			
	    			self.tags = Tag.tagsAll.query(function() {
			    		self.selectedTagNameToChange = self.tags[0].tagName;
	    				self.newTagName = null;
	    				$window.alert('Tag has been successfully added.');
	    			});
	    		});
	    	}
	    	
	    	self.editTag = function() {
	    		self.oldValueSelected = self.selectedTagNameToChange;
	    	}
	    	
	      }
	    ]
	  });
	
}) ()