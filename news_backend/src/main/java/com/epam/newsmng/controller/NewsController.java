package com.epam.newsmng.controller;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.newsmng.domain.News;
import com.epam.newsmng.service.impl.NewsServiceImpl;

/**
 * The Class NewsController.
 */
@Controller
//@CrossOrigin(origins = "http://localhost:8001")
@RequestMapping(value = "/news", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class NewsController {

	/** The news service. */
	@Autowired
	private NewsServiceImpl newsService;
	
	/**
	 * Instantiates a new news controller.
	 */
	public NewsController() {
	}
	
	/**
	 * Post news.
	 *
	 * @param news the news
	 * @return the long
	 */
	@RequestMapping(value = {"/"}, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Long postNews(@RequestBody @NotNull @Valid News news) {
		return newsService.add(news);
	}

	/**
	 * Gets the news.
	 *
	 * @param newsId the news id
	 * @return the news
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public News getNews(@PathVariable("id") @Min(1) @Max(Long.MAX_VALUE) long newsId) {
		return newsService.getById(newsId);
	}

	/**
	 * Gets the list of news.
	 *
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the list of news
	 */
	@RequestMapping(value = {"", "/"}, params = {"startPos", "offset"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Collection<News> getListOfNews(
			@RequestParam(value = "startPos", defaultValue = "0") @Min(0) @Max(Integer.MAX_VALUE) int startPos,
			@RequestParam(value = "offset", defaultValue = "10") @Min(10) @Max(Integer.MAX_VALUE) int offset) {
		if (true) {throw new RuntimeException("upssss");}
		return newsService.getAll(startPos, offset);
	}

	/**
	 * Updates the news.
	 *
	 * @param news the news
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public void updateNews(@RequestBody @NotNull @Valid News news) {
		newsService.update(news);
	}

	/**
	 * Deletes the news.
	 *
	 * @param newsId the news id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void deleteNews(@PathVariable("id") @Min(1) @Max(Long.MAX_VALUE) long newsId) {
		newsService.deleteById(newsId);
	}

	/**
	 * Gets the news by author.
	 *
	 * @param authorId the author id
	 * @param startPos the start pos
	 * @param offset the offset
	 * @return the news by author
	 */
	@RequestMapping(value = "/", params = {"authorId", "startPos", "offset"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Collection<News> getNewsByAuthor(
			@RequestParam(value = "authorId", required = true) @Min(1) @Max(Long.MAX_VALUE) long authorId,
			@RequestParam(value = "startPos", defaultValue = "0") @Min(0) @Max(Integer.MAX_VALUE) int startPos,
			@RequestParam(value = "offset", defaultValue = "10") @Min(10) @Max(Integer.MAX_VALUE) int offset) {
		return newsService.getNewsByAuthor(authorId, startPos, offset);
	}
	
	/**
	 * Gets the news by tag.
	 *
	 * @param tagName the tag name
	 * @param startPos the start pos
	 * @param offset the offset
	 * @return the news by tag
	 */
	@RequestMapping(value = "/", params = {"tagName", "startPos", "offset"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Collection<News> getNewsByTag(
			@RequestParam(value = "tagName", required = true) @NotNull String tagName,
			@RequestParam(value = "startPos", defaultValue = "0") @Min(0) @Max(Integer.MAX_VALUE) int startPos,
			@RequestParam(value = "offset", defaultValue = "10") @Min(10) @Max(Integer.MAX_VALUE) int offset) {
		return newsService.getNewsByTag(tagName, startPos, offset);
	}
	
	/**
	 * Gets the top N commented news.
	 *
	 * @param topCommentedNewsAmount the top commented news amount
	 * @param startPos the start pos
	 * @param offset the offset
	 * @return the top N commented news
	 */
	@RequestMapping(value = "/", params = {"topCommentedNewsAmount", "startPos", "offset"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Collection<News> getTopNCommentedNews(
			@RequestParam(value = "topCommentedNewsAmount", required = true) @Min(2) @Max(100) int topCommentedNewsAmount,
			@RequestParam(value = "startPos", defaultValue = "0") @Min(0) @Max(Integer.MAX_VALUE) int startPos,
			@RequestParam(value = "offset", defaultValue = "10") @Min(10) @Max(Integer.MAX_VALUE) int offset) {
		return newsService.getTopNCommentedNews(topCommentedNewsAmount, startPos, offset);
	}
}
