package com.epam.newsmng.controller;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.newsmng.domain.Comment;
import com.epam.newsmng.service.impl.CommentServiceImpl;

/**
 * The Class CommentController.
 */
@Controller
@RequestMapping(value = "/news/{newsId}/comments", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class CommentController {
	
	/**
	 * Instantiates a new comment controller.
	 */
	public CommentController() {
	}

	/** The comment service. */
	@Autowired
	private CommentServiceImpl commentService;

	/**
	 * Post comment.
	 *
	 * @param comment the comment
	 * @return the long
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Long postComment(
			@PathVariable("newsId") @Min(1) @Max(Long.MAX_VALUE) long newsId,
			@RequestBody @NotNull @Valid Comment comment) {
		comment.setNewsId(newsId);
		return commentService.add(comment);
	}

	/**
	 * Gets the comment.
	 *
	 * @param commentId the comment id
	 * @return the comment
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Comment getComment(@PathVariable("id") @Min(1) @Max(Long.MAX_VALUE) long commentId) {
		return commentService.getById(commentId);
	}
	
	/**
	 * Gets the list of all comments by news id.
	 *
	 * @param newsId the news id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the list of comments
	 */
	@RequestMapping(value = {"", "/"}, params = {"startPos", "offset"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Collection<Comment> getCommentsByNewsId(
			@PathVariable("newsId") @Min(1) @Max(Long.MAX_VALUE) long newsId,
			@RequestParam(value = "startPos", defaultValue = "0") @Min(0) @Max(Integer.MAX_VALUE) int startPos,
			@RequestParam(value = "offset", defaultValue = "10") @Min(10) @Max(Integer.MAX_VALUE) int offset) {
		return commentService.getCommentsByNewsId(newsId, startPos, offset);
	}

	/**
	 * Update comment.
	 *
	 * @param comment the comment
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public void updateComment(@RequestBody @NotNull @Valid Comment comment) {
		commentService.update(comment);
	}

	/**
	 * Delete comment.
	 *
	 * @param commentId the comment id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void deleteComment(@PathVariable("id") @Min(1) @Max(Long.MAX_VALUE) long commentId) {
		commentService.deleteById(commentId);
	}
}
