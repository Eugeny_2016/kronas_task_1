package com.epam.newsmng.controller;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.newsmng.domain.Author;
import com.epam.newsmng.domain.AuthorTO;
import com.epam.newsmng.service.impl.AuthorServiceImpl;

/**
 * The Class AuthorController.
 */
@Controller
@RequestMapping(value = "/authors", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthorController {

	/**
	 * Instantiates a new author controller.
	 */
	public AuthorController() {
	}

	/** The author service. */
	@Autowired
	private AuthorServiceImpl authorServ;

	/**
	 * Post author.
	 *
	 * @param author the author
	 * @return the long
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Long postAuthor(@RequestBody @NotNull @Valid Author author) {
		return authorServ.add(author);
	}

	/**
	 * Gets the author.
	 *
	 * @param authorId the author id
	 * @return the author
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Author getAuthor(@PathVariable("id") @Min(1) @Max(Long.MAX_VALUE) long authorId) {
		return authorServ.getById(authorId);
	}

	/**
	 * Gets the list of authors.
	 *
	 * @return the list of authors
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Collection<Author> getListOfAuthors() {
		return authorServ.getAll();
	}

	/**
	 * Update author.
	 *
	 * @param author the author
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public void updateAuthor(@RequestBody @NotNull @Valid Author author) {
		authorServ.update(author);
	}

	/**
	 * Delete author.
	 *
	 * @param authorId the author id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void deleteAuthor(@PathVariable("id") @Min(1) @Max(Long.MAX_VALUE) long authorId) {
		authorServ.deleteById(authorId);
	}

	/**
	 * Gets the amount of news per author.
	 *
	 * @param isOnlyAmountOfNews the is only amount of news
	 * @return the amount of news per author
	 */
	@RequestMapping(value = "/", params = {"isOnlyAmountOfNews"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Collection<AuthorTO> getAmountOfNewsPerAuthor(
			@RequestParam(value = "isOnlyAmountOfNews", required = true) @AssertTrue boolean isOnlyAmountOfNews) {
		return authorServ.getAmountOfNewsPerAuthor();
	}
}
