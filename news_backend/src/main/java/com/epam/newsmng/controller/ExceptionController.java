package com.epam.newsmng.controller;

import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.newsmng.domain.ErrorReport;

/**
 * The Class ExceptionController.
 */
@ControllerAdvice
public class ExceptionController {

	/**
	 * Handles exception of class Exception.
	 *
	 * @param ex the ex
	 * @return the error report
	 */
	@ExceptionHandler(value = RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorReport handleSomeException(Exception ex) {
		ErrorReport errorReport = new ErrorReport();
		errorReport.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorReport.setMessage(ex.getMessage());
		
		return errorReport;
	}
	
	/**
	 * Handles exception of class ObjectOptimisticLockingFailureException.
	 *
	 * @param ex the ex
	 * @return the error report
	 */
	@ExceptionHandler(value = ObjectOptimisticLockingFailureException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorReport handleSomeException(ObjectOptimisticLockingFailureException ex) {
		ErrorReport errorReport = new ErrorReport();
		errorReport.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorReport.setMessage(ex.getMessage());
		
		return errorReport;
	}	
}
