package com.epam.newsmng.controller;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.newsmng.domain.Tag;
import com.epam.newsmng.domain.TagTO;
import com.epam.newsmng.service.impl.TagServiceImpl;

/**
 * The Class TagController.
 */
@Controller
@RequestMapping(value = "/tags", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class TagController {

	/**
	 * Instantiates a new tag controller.
	 */
	public TagController() {
	}

	/** The tag service. */
	@Autowired
	private TagServiceImpl tagService;

	/**
	 * Posts tag.
	 *
	 * @param tag the tag
	 * @return the long
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Tag postTag(@RequestBody @NotNull @Valid Tag tag) {
		return tagService.save(tag);
	}

	/**
	 * Gets the tag by name.
	 *
	 * @param tagName the tag name
	 * @return the tag
	 */
	@RequestMapping(value = "/{tagName}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Tag getTag(@PathVariable("tagName") @NotNull String tagName) {
		return tagService.getByName(tagName);
	}

	/**
	 * Gets the list of tags.
	 *
	 * @return the list of tags
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Collection<Tag> getListOfTags() {
		return tagService.getAll();
	}

	/**
	 * Updates tag.
	 *
	 * @param oldTagName the old tag name
	 * @param newTag the new tag
	 */
	@RequestMapping(value = "/{tagName}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public void updateTag(@PathVariable("tagName") @NotNull String oldTagName, 
						  @RequestBody @NotNull @Valid Tag newTag) {
		tagService.update(oldTagName, newTag);
	}

	/**
	 * Deletes tag.
	 *
	 * @param tag the tag
	 */
	@RequestMapping(value = "/{tagName}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void deleteTag(@PathVariable("tagName") @NotNull String tagName) {
		tagService.delete(tagName);
	}

	/**
	 * Gets the amount of news per tag.
	 *
	 * @param isOnlyAmountOfNews the is only amount of news
	 * @return the amount of news per tag
	 */
	@RequestMapping(value = "/", params = {"isOnlyAmountOfNews"}, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Collection<TagTO> getAmountOfNewsPerTag(
			@RequestParam(value = "isOnlyAmountOfNews", required = true) @AssertTrue boolean isOnlyAmountOfNews) {
		return tagService.getAmountOfNewsPerTag();
	}
}
