package com.epam.newsmng.dao;

import java.util.Collection;

import com.epam.newsmng.domain.Tag;
import com.epam.newsmng.domain.TagTO;

/**
 * The Interface TagDAO.
 */
public interface TagDAO extends CrudDAO<Tag> {
	
	/**
	 * Persists the tag.
	 *
	 * @param tag the tag
	 * @return the tag
	 */
	Tag persist(Tag tag);
	
	/**
	 * Retrieves all tags without start position and offset.
	 *
	 * @return the collection of tags
	 */
	Collection<Tag> retrieveAll();
	
	/**
	 * Fetches tag by name.
	 *
	 * @param tagName the tag name
	 * @return the tag
	 */
	Tag fetchByName(String tagName);
	
	/**
	 * Updates the old tag name with the new one.
	 *
	 * @param oldTagName the old tag name
	 * @param newTag the new tag
	 */
	void update(String oldTagName, Tag newTag);
	
	/**
	 * Removes the tag.
	 *
	 * @param tagName the tag name
	 */
	void remove(String tagName);
	
	/**
	 * Retrieves the amount of news per tag.
	 *
	 * @return the map
	 */
	Collection<TagTO> retrieveAmountOfNewsPerTag();
}
