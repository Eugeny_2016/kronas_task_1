package com.epam.newsmng.dao;

import java.util.Collection;

import com.epam.newsmng.domain.Author;
import com.epam.newsmng.domain.AuthorTO;

/**
 * The Interface AuthorDAO.
 */
public interface AuthorDAO extends CrudDAO<Author> {
	
	/**
	 * Retrieves all authors without start position and offset.
	 *
	 * @return the collection of authors
	 */
	Collection<Author> retrieveAll();
	
	/**
	 * Retrieves amount of news per author.
	 *
	 * @return the map
	 */
	Collection<AuthorTO> retrieveAmountOfNewsPerAuthor();		
}
