package com.epam.newsmng.dao.impl;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.epam.newsmng.dao.CommentDAO;
import com.epam.newsmng.domain.Comment;
import com.epam.newsmng.domain.News;

@Repository
public class CommentDAOImpl implements CommentDAO {

	@Value("${comment.text}")
	private String defaultCensureText;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void setDefaultCensureText(String defaultCensureText) {
		this.defaultCensureText = defaultCensureText;
	}
	
	@Override
	public Long save(Comment comment) {
		// Doing so because of the @JsonIgnore annotation above the news field in comment. 
		// So I can't get from UI, it will be ignored.
		News news = entityManager.find(News.class, comment.getNewsId());
		comment.setNews(news);
		entityManager.persist(comment);
		return comment.getCommentId();
	}

	@Override
	public Comment retrieveById(Long id) {
		return entityManager.find(Comment.class, id);
	}

	@Override
	public Collection<Comment> retrieveAll(int startPos, int offset) {
		return entityManager.createQuery("from Comment", Comment.class)
				.setFirstResult(startPos)
				.setMaxResults(offset)
				.getResultList();
	}

	@Override
	public void update(Comment comment) {
		entityManager.merge(comment);		
	}

	@Override
	public void deleteById(Long id) {
		Comment commentToDelete = entityManager.find(Comment.class, id);
		if (commentToDelete != null) {
			entityManager.remove(commentToDelete);
		}
	}
	
	@Override
	public Collection<Comment> retrieveCommentsByNewsId(long newsId, int startPos, int offset) {
		return entityManager.createQuery("select C from Comment C join C.news N where N.newsId = :newsId "
				   					   + "order by C.creationDate desc", Comment.class)
				.setParameter("newsId", newsId)
				.setFirstResult(startPos)
				.setMaxResults(offset)
				.getResultList();
	}
	
	@Override
	public void updateCommentWithCensuring(Long commentId, String messageWithReason) {
		Comment comment = entityManager.find(Comment.class, commentId);
		comment.setCommentText(defaultCensureText + "\n" + messageWithReason);
		entityManager.merge(comment);
	}
}
