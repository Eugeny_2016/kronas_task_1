package com.epam.newsmng.dao;

import java.util.Collection;

/**
 * The Interface CrudDAO.
 *
 * @param <T> the generic type
 */
public interface CrudDAO <T> {
	
	/**
	 * Save.
	 *
	 * @param obj the obj
	 * @return the long
	 */
	Long save(T obj);
	
	/**
	 * Retrieve by id.
	 *
	 * @param id the id
	 * @return the t
	 */
	T retrieveById(Long id);
	
	/**
	 * Retrieve all.
	 *
	 * @param startPos the start pos
	 * @param offset the offset
	 * @return the collection
	 */
	Collection<T> retrieveAll(int startPos, int offset);
	
	/**
	 * Update.
	 *
	 * @param obj the obj
	 */
	void update(T obj);

	/**
	 * Delete by id.
	 *
	 * @param id the id
	 */
	void deleteById(Long id);
}
