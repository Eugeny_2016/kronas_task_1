package com.epam.newsmng.dao;

import java.util.Collection;

import com.epam.newsmng.domain.Comment;

/**
 * The Interface CommentDAO.
 */
public interface CommentDAO extends CrudDAO<Comment> {
	
	/**
	 * Retrieves comments by news id.
	 *
	 * @param newsId the news id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return comments by news
	 */
	Collection<Comment> retrieveCommentsByNewsId(long newsId, int startPos, int offset);
	
	/**
	 * Censures the comment by id.
	 *
	 * @param commentId the comment id
	 * @param messageWithReason the message with reason
	 */
	void updateCommentWithCensuring(Long commentId, String messageWithReason);
}
