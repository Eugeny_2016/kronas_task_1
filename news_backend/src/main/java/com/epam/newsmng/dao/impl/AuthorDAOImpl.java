package com.epam.newsmng.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.newsmng.domain.Author;
import com.epam.newsmng.domain.AuthorTO;

@Repository
public class AuthorDAOImpl extends AbstractAuthorDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public Long save(Author author) {
		entityManager.persist(author);
		return author.getAuthorId();
	}

	@Override
	public Author retrieveById(Long id) {
		return entityManager.find(Author.class, id);
	}

	@Override
	public Collection<Author> retrieveAll() {
		return entityManager.createQuery("from Author", Author.class)
				.getResultList(); 
	}

	@Override
	public void update(Author author) {
		entityManager.merge(author);
	}

	@Override
	public void deleteById(Long id) {
		Author authorToDelete = entityManager.find(Author.class, id);
		if (authorToDelete != null) {
			entityManager.remove(authorToDelete);
		}
	}

	@Override
	public Collection<AuthorTO> retrieveAmountOfNewsPerAuthor() {
		Collection<AuthorTO> authorsTO = new ArrayList<>();
		List<Author> authors = entityManager.createQuery("from Author", Author.class).getResultList();
		
		for(Author author : authors) {
			int size = 0;
			if ((size = author.getNews().size()) > 0) {
				AuthorTO authorTO = new AuthorTO(author, size);
				authorsTO.add(authorTO);
			}
		}
		return authorsTO;
	}
}
