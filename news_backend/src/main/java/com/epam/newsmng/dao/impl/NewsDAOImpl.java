package com.epam.newsmng.dao.impl;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.newsmng.dao.NewsDAO;
import com.epam.newsmng.domain.News;

@Repository
public class NewsDAOImpl implements NewsDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public Long save(News news) {
		News newsPersisted = entityManager.merge(news);
		return newsPersisted.getNewsId();
	}
	
	@Override
	public News retrieveById(Long id) {
		return entityManager.find(News.class, id);
	}

	@Override
	public Collection<News> retrieveAll(int startPos, int offset) {
		return entityManager.createQuery("select N from News N order by N.creationDate desc", News.class)
				.setFirstResult(startPos)
				.setMaxResults(offset)
				.getResultList();
	}

	@Override
	public void update(News news) {
		entityManager.merge(news);
	}

	@Override
	public void deleteById(Long newsId) {
		News newsToDelete = entityManager.find(News.class, newsId);
		if (newsToDelete != null) {
			newsToDelete.getAuthors().clear();
			newsToDelete.getTags().clear();
			entityManager.merge(newsToDelete);
			entityManager.remove(newsToDelete);
		}
	}

	@Override
	public Collection<News> retrieveNewsByAuthor(Long authorId, int startPos, int offset) {
		return entityManager.createQuery("select N from News N join N.authors A where A.authorId = :authorId", News.class)
				.setParameter("authorId", authorId)
				.setFirstResult(startPos)
				.setMaxResults(offset)
				.getResultList();
	}

	@Override
	public Collection<News> retrieveNewsByTag(String tagName, int startPos, int offset) {
		return entityManager.createQuery("select N from News N join N.tags T where T.tagName = :tagName", News.class)
				.setParameter("tagName", tagName)
				.setFirstResult(startPos)
				.setMaxResults(offset)
				.getResultList();
	}

	@Override
	public Collection<News> retrieveTopNCommentedNews(int topCommentedNewsAmount, int startPos, int offset) {
		int realOffset = (offset < topCommentedNewsAmount) ? offset : topCommentedNewsAmount;
		
		Collection<News> news = entityManager.createQuery("select new News (N.newsId, N.title, N.shortText, N.fullText, N.creationDate, N.modificationDate) "
									   					+ "from News N "
									   					+ "left join N.comments C "
									   					+ "group by N.newsId, N.title, N.shortText, N.fullText, N.creationDate, N.modificationDate "
									   					+ "order by count(C.newsId) desc", News.class)
				.setFirstResult(startPos)
				.setMaxResults(realOffset)
				.getResultList();
		
		Collection<News> fullNews = new ArrayList<>();
		for (News newsItem : news) {
			fullNews.add(entityManager.find(News.class, newsItem.getNewsId()));
		}
		return fullNews;
	}
}
