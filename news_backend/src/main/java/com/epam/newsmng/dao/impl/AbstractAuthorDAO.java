package com.epam.newsmng.dao.impl;

import java.util.Collection;

import com.epam.newsmng.dao.AuthorDAO;
import com.epam.newsmng.domain.Author;

public abstract class AbstractAuthorDAO implements AuthorDAO {
	
	@Override
	public Collection<Author> retrieveAll(int startPos, int offset) {
		throw new UnsupportedOperationException("There is no pagging for authors.");
	}
}
