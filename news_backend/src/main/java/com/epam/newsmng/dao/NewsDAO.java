package com.epam.newsmng.dao;

import java.util.Collection;

import com.epam.newsmng.domain.News;

/**
 * The Interface NewsDAO.
 */
public interface NewsDAO extends CrudDAO<News> {
	
	/**
	 * Retrieves comments by news id.
	 *
	 * @param newsId the news id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return comments by news
	 *//*
	Collection<Comment> retrieveCommentsByNewsId(long newsId, int startPos, int offset);*/
	
	/**
	 * Retrieves news by author.
	 *
	 * @param authorId the author id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the collection
	 */
	Collection<News> retrieveNewsByAuthor(Long authorId, int startPos, int offset);
	
	/**
	 * Retrieves news by tag.
	 *
	 * @param tagId the tag id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the collection
	 */
	Collection<News> retrieveNewsByTag(String tagName, int startPos, int offset);
	
	/**
	 * Retrieves top N commented news.
	 *
	 * @param topCommentedNewsAmount the top commented news amount
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the collection
	 */
	Collection<News> retrieveTopNCommentedNews(int topCommentedNewsAmount, int startPos, int offset);
}
