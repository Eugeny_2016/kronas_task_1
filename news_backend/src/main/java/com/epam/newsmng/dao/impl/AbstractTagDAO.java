package com.epam.newsmng.dao.impl;

import java.util.Collection;

import com.epam.newsmng.dao.TagDAO;
import com.epam.newsmng.domain.Tag;

public abstract class AbstractTagDAO implements TagDAO {

	@Override
	public Long save(Tag tag) {
		throw new UnsupportedOperationException("There is no tag id. Only tag name");
	}

	@Override
	public Tag retrieveById(Long id) {
		throw new UnsupportedOperationException("There is no tag id. Only tag name");
	}
	
	@Override
	public Collection<Tag> retrieveAll(int startPos, int offset) {
		throw new UnsupportedOperationException("There is no pagging for tags.");
	}
	
	@Override
	public void update(Tag tag) {
		throw new UnsupportedOperationException("Can't update tag without the old tagname");
	}
	
	@Override
	public void deleteById(Long id) {
		throw new UnsupportedOperationException("There is no tag id. Only tag name");
	}
}
