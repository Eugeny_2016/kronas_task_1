package com.epam.newsmng.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.newsmng.domain.Tag;
import com.epam.newsmng.domain.TagTO;

@Repository
public class TagDAOImpl extends AbstractTagDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public Tag persist(Tag tag) {
		entityManager.persist(tag);
		return tag;
	}
	
	@Override
	public Collection<Tag> retrieveAll() {
		return entityManager.createQuery("from Tag", Tag.class)
				.getResultList();
	}
	
	@Override
	public Tag fetchByName(String tagName) {
		return entityManager.find(Tag.class, tagName);
	}

	@Override
	public Collection<Tag> retrieveAll(int startPos, int offset) {
		return entityManager.createQuery("from Tag", Tag.class)
				.setFirstResult(startPos)
				.setMaxResults(offset)
				.getResultList();
	}
	
	@Override
	public void update(String oldTagName, Tag newTag) {
		Tag tag = entityManager.createQuery("from Tag T where T.tagName = :oldTagName", Tag.class)
					.setParameter("oldTagName", oldTagName)
					.getSingleResult();
		tag.setTagName(newTag.getTagName());
		entityManager.merge(tag);
	}
	
	@Override
	public void remove(String tagName) {
		entityManager.createQuery("delete from Tag T where T.tagName = :tagName")
			.setParameter("tagName", tagName)
			.executeUpdate();
	}

	@Override
	public Collection<TagTO> retrieveAmountOfNewsPerTag() {
		Collection<TagTO> tagsTO = new ArrayList<>();
		List<Tag> tags = entityManager.createQuery("from Tag", Tag.class).getResultList();
	
		for(Tag tag : tags) {
			int size = 0;
			if ((size = tag.getNews().size()) > 0) {
				TagTO tagTo = new TagTO(tag, size);
				tagsTO.add(tagTo);
			}
		}		
		return tagsTO;
	}
}
