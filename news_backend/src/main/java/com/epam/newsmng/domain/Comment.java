package com.epam.newsmng.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.epam.newsmng.utils.LocalDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="COMMENTS")
public class Comment implements Serializable, Comparable<Comment> {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "CommentsSequence", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CommentsSequence")
	@Column(name = "COMMENT_ID", nullable = false, updatable = false)
	private Long commentId;
	
	@Transient
	private Long newsId;
	
	@NotNull
	@Size(min = 5, max = 100)
	@Column(name = "COMMENT_TEXT", nullable = false)
	private String commentText;

	@Column(name = "CREATION_DATE", nullable = false, updatable = false)
	private LocalDateTime creationDate;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEWS_NEWS_ID", referencedColumnName = "NEWS_ID", updatable=false)
	private News news;

	public Comment() {
	}

	public Comment(String commentText, LocalDateTime creationDate) {
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public Comment(Long commentId, String commentText, LocalDateTime creationDate) {
		this.commentId = commentId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}
	
	@PrePersist
	protected void setCreationDate() {
		this.creationDate = LocalDateTime.now();
	}

	public Long getCommentId() {
		return commentId;
	}
	
	public Long getNewsId() {
		return newsId;
	}

	public String getCommentText() {
		return commentText;
	}
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	public LocalDateTime getCreationDate() {
		return creationDate;
	}
	
	public News getNews() {
		return news;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
	
	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Comment [commentId=");
		sb.append(commentId);
		sb.append(", newsId=");
		sb.append(newsId);
		sb.append(", commentText=");
		sb.append(commentText);
		sb.append(", creationDate=");
		sb.append(creationDate);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {		
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Comment other = (Comment) obj;
		if (commentId == null) {
			if (other.commentId != null) {
				return false;
			}
		} else if (!commentId.equals(other.commentId)) {
			return false;
		}
		
		if (creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else if (!creationDate.equals(other.creationDate)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Comment comment) {
		int result = 0;
		
		if (this.creationDate.isAfter(comment.creationDate)) {
			result = 1;
		} else if(this.creationDate.isBefore(comment.creationDate)) {
			result = -1;
		}		
		return result;
	}
}
