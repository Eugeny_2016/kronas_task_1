package com.epam.newsmng.domain;

import java.io.Serializable;

public class AuthorTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Author author;
	private Integer count;
	
	public AuthorTO() {
	}
	
	public AuthorTO(Author author, Integer count) {
		this.author = author;
		this.count = count;
	}

	public Author getAuthor() {
		return author;
	}
	
	public void setAuthor(Author author) {
		this.author = author;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "AuthorTO [author=" + author + ", count=" + count + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((count == null) ? 0 : count.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}
		
		AuthorTO other = (AuthorTO) obj;
		if (author == null) {
			if (other.author != null) {
				return false;
			}
		} else if (!author.equals(other.author)) {
			return false;
		}
		if (count == null) {
			if (other.count != null) {
				return false;
			}
		} else if (!count.equals(other.count)) {
			return false;
		}
		return true;
	}
}
