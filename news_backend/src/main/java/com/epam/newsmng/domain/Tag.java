package com.epam.newsmng.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TAG")
public class Tag implements Serializable, Comparable<Tag> {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "TagSequence", sequenceName = "TAG_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TagSequence")
	@Column(name = "TAG_ID", nullable = false, updatable = false)
	private Long tagId;
	
	@NotNull
	@Column(name = "TAG_NAME", nullable = false)
	private String tagName;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags", cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<News> news;

	public Tag() {
	}
	
	public Tag(String tagName) {
		this.tagName = tagName;
	}
		
	public Long getTagId() {
		return tagId;
	}

	public String getTagName() {
		return tagName;
	}
	
	public Set<News> getNews() {
		return news;
	}
	
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	public void setNews(Set<News> news) {
		this.news = news;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tag [tagId=");
		sb.append(tagId);
		sb.append(", tagName=");
		sb.append(tagName);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Tag other = (Tag) obj;
		if (tagId == null) {
			if (other.tagId != null) {
				return false;
			}
		} else if (!tagId.equals(other.tagId)) {
			return false;
		}
		if (tagName == null) {
			if (other.tagName != null) {
				return false;
			}
		} else if (!tagName.equals(other.tagName)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Tag tag) {
		return this.tagName.compareTo(tag.tagName);
	}
}
