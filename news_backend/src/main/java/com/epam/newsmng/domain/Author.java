package com.epam.newsmng.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable, Comparable<Author> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "AuthorSequence", sequenceName = "AUTHOR_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AuthorSequence")
	@Column(name = "AUTHOR_ID", nullable = false, updatable = false)
	private Long authorId;
	
	@NotNull
	@Size(min = 5, max = 30)
	@Column(name = "AUTHOR_NAME", nullable = false)
	private String authorName;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "authors", cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	private Set<News> news;

	public Author() {
	}

	public Author(String authorName) {
		this.authorName = authorName;
	}

	public Author(Long authorId, String authorName) {
		this.authorId = authorId;
		this.authorName = authorName;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Author [authorId=");
		sb.append(authorId);
		sb.append(", authorName=");
		sb.append(authorName);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null) {
				return false;
			}
		} else if (!authorId.equals(other.authorId)) {
			return false;
		}

		if (authorName == null) {
			if (other.authorName != null) {
				return false;
			}
		} else if (!authorName.equals(other.authorName)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Author author) {
		return this.authorName.compareTo(author.authorName);
	}
}
