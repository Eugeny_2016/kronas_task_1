package com.epam.newsmng.domain;

public class ErrorReport {
	
	private int httpStatus;
	
	private int errorCode;
	
	private String message;
	
	public ErrorReport() {
	}
	
	public ErrorReport(int httpStatus, String message) {
		this.httpStatus = httpStatus;
		this.message = message;
	}
	
	public ErrorReport(int httpStatus, int errorCode, String message) {
		this.httpStatus = httpStatus;
		this.errorCode = errorCode;
		this.message = message;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
