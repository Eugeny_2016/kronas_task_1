package com.epam.newsmng.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;


@Entity
@Table(name = "NEWS")
public class News implements Serializable, Comparable<News> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "NewsSequence", sequenceName = "NEWS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NewsSequence")
	@Column(name = "NEWS_ID", nullable = false, updatable = false)
	private Long newsId;
	
	@NotNull
	@Size(min = 3, max = 30)
	@Column(name = "TITLE", nullable = false)
	private String title;
	
	@NotNull
	@Size(min = 10, max = 100)
	@Column(name = "SHORT_TEXT", nullable = false)
	private String shortText;
	
	@NotNull
	@Size(min = 10, max = 2000)
	@Column(name = "FULL_TEXT", nullable = false)
	private String fullText;
	
	@Column(name = "CREATION_DATE", nullable = false, updatable = false)
	private LocalDateTime creationDate;
	
	@Version
	@Column(name = "MODIFICATION_DATE", nullable = false)
	private LocalDateTime modificationDate;
	
	@NotNull
	@Valid
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_AUTHOR", 
						joinColumns = {@JoinColumn(name = "NEWS_NEWS_ID", nullable = false, updatable = false) }, 
						inverseJoinColumns = {@JoinColumn(name = "AUTHOR_AUTHOR_ID", nullable = false, updatable = false) })
	private Set<Author> authors;

	@NotNull
	@Valid
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_TAG", 
						joinColumns = {@JoinColumn(name = "NEWS_NEWS_ID", nullable = false, updatable = false) }, 
						inverseJoinColumns = {@JoinColumn(name = "TAG_TAG_ID", nullable = false, updatable = false) })
	private Set<Tag> tags;
	
	@Valid
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "news", cascade = CascadeType.ALL)
	private Set<Comment> comments;

	public News() {
	}

	public News(String title, String shortText, String fullText, LocalDateTime creationDate,
			LocalDateTime modificationDate) {
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(Long newsId, String title, String shortText, String fullText, LocalDateTime creationDate,
			LocalDateTime modificationDate) {
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}
	
	@PrePersist
	protected void setCreationDate() {
		this.creationDate = LocalDateTime.now();
		// Not needed any more, because of @Version annotation
		//this.modificationDate = this.creationDate;
	}
	
	// Not needed any more, because of @Version annotation 
	/* @PreUpdate
	protected void setModificationDate() {
		this.modificationDate = LocalDateTime.now();
	}*/

	public Long getNewsId() {
		return newsId;
	}

	public String getTitle() {
		return title;
	}

	public String getShortText() {
		return shortText;
	}

	public String getFullText() {
		return fullText;
	}
	
	@JsonSerialize(using = com.epam.newsmng.utils.LocalDateTimeSerializer.class)
	public LocalDateTime getCreationDate() {
		return creationDate;
	}
	
	@JsonSerialize(using = com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime getModificationDate() {
		return modificationDate;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}
	
	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public void setModificationDate(LocalDateTime modificationDate) {
		this.modificationDate = modificationDate;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("News [newsId=");
		sb.append(newsId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", shortText=");
		sb.append(shortText);
		sb.append(", creationDate=");
		sb.append(creationDate);
		sb.append(", modificationDate=");
		sb.append(modificationDate);
		sb.append(", setOfAuthors=");
		sb.append(authors);
		sb.append(", setOfTags=");
		sb.append(tags);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		News other = (News) obj;

		if (newsId == null) {
			if (other.newsId != null) {
				return false;
			}
		} else if (!newsId.equals(other.newsId)) {
			return false;
		}

		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(News news) {
		int result = 0;

		if (this.creationDate.isAfter(news.creationDate)) {
			result = 1;
		} else if (this.creationDate.isBefore(news.creationDate)) {
			result = -1;
		}
		return result;
	}
}
