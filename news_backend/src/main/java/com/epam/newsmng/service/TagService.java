package com.epam.newsmng.service;

import java.util.Collection;

import com.epam.newsmng.domain.Tag;
import com.epam.newsmng.domain.TagTO;

/**
 * The Interface TagService.
 */
public interface TagService extends IService<Tag> {
	
	/**
	 * Saves the tag.
	 *
	 * @param tag the tag
	 * @return the tag
	 */
	Tag save(Tag tag);
	
	/**
	 * Gets all tags without paging.
	 *
	 * @return the collection of tags
	 */
	Collection<Tag> getAll();
	
	/**
	 * Gets tag by name.
	 *
	 * @param tagName the tag name
	 * @return the tag
	 */
	Tag getByName(String tagName);
	
	/**
	 * Updates the old tag name with the new one.
	 *
	 * @param oldTagName the old tag name
	 * @param newTag the new tag
	 */
	void update(String oldTagName, Tag newTag);
	
	/**
	 * Deletes the tag.
	 *
	 * @param tagName the tag name
	 */
	void delete(String tagName);
	
	/**
	 * Gets the amount of news per tag.
	 *
	 * @return the amount of news per tag
	 */
	Collection<TagTO> getAmountOfNewsPerTag();
}
