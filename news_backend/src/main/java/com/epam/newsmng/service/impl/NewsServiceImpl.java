package com.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.NewsDAO;
import com.epam.newsmng.domain.News;
import com.epam.newsmng.service.NewsService;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsDAO newsDAO;
	
	@Override
	public Long add(News news) {
		return newsDAO.save(news);
	}

	@Override
	public News getById(Long newsId) {
		return newsDAO.retrieveById(newsId);
	}

	@Override
	public Collection<News> getAll(int startPos, int offset) {
		return newsDAO.retrieveAll(startPos, offset);
	}
	
	@Override
	public void update(News news) {
		News newsPersistent = newsDAO.retrieveById(news.getNewsId());
		news.setCreationDate(newsPersistent.getCreationDate());
		newsDAO.update(news);
	}
	
	@Override
	public void deleteById(Long newsId) {
		newsDAO.deleteById(newsId);
	}
	
	/*@Override
	public Collection<Comment> getCommentsByNewsId(long newsId, int startPos, int offset) {
		return newsDAO.retrieveCommentsByNewsId(newsId, startPos, offset);
	}*/

	@Override
	public Collection<News> getNewsByAuthor(Long authorId, int startPos, int offset) {
		return newsDAO.retrieveNewsByAuthor(authorId, startPos, offset);
	}

	@Override
	public Collection<News> getNewsByTag(String tagName, int startPos, int offset) {
		return newsDAO.retrieveNewsByTag(tagName, startPos, offset);
	}

	@Override
	public Collection<News> getTopNCommentedNews(int topCommentedNewsAmount, int startPos, int offset) {
		return newsDAO.retrieveTopNCommentedNews(topCommentedNewsAmount, startPos, offset);
	}
}
