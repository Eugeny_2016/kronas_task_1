package com.epam.newsmng.service;

import java.util.Collection;

import com.epam.newsmng.domain.Author;
import com.epam.newsmng.domain.AuthorTO;

/**
 * The Interface AuthorService.
 */
public interface AuthorService extends IService<Author> {
	
	/**
	 * Gets all authors without paging.
	 *
	 * @return the collection of authors
	 */
	Collection<Author> getAll();
	
	/**
	 * Gets the amount of news per author.
	 *
	 * @return the amount of news per author
	 */
	Collection<AuthorTO> getAmountOfNewsPerAuthor();	
}
