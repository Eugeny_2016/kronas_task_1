package com.epam.newsmng.service;

import java.util.Collection;

import com.epam.newsmng.domain.Comment;

/**
 * The Interface CommentService.
 */
public interface CommentService extends IService<Comment> {
	
	/**
	 * Gets comments by news id.
	 *
	 * @param newsId the news id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return comments by news
	 */
	Collection<Comment> getCommentsByNewsId(long newsId, int startPos, int offset);
	
	/**
	 * Censures the comment by id.
	 *
	 * @param commentId the comment id
	 * @param messageWithReason the message with reason
	 */
	void censureComment(Long commentId, String messageWithReason);
}
