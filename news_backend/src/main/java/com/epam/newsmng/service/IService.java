package com.epam.newsmng.service;

import java.util.Collection;

/**
 * The Interface IService.
 *
 * @param <T> the generic type
 */
public interface IService<T> {
	
	/**
	 * Adds the.
	 *
	 * @param obj the obj
	 * @return the long
	 */
	Long add(T obj);

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	T getById(Long id);

	/**
	 * Gets the all.
	 *
	 * @param startPos the start pos
	 * @param offset the offset
	 * @return the all
	 */
	Collection<T> getAll(int startPos, int offset);

	/**
	 * Update.
	 *
	 * @param obj the obj
	 */
	void update(T obj);

	/**
	 * Delete by id.
	 *
	 * @param id the id
	 */
	void deleteById(Long id);
}
