package com.epam.newsmng.service.impl;

import java.util.Collection;

import com.epam.newsmng.domain.Tag;
import com.epam.newsmng.service.TagService;

public abstract class AbstractTagService implements TagService {
	
	@Override
	public Long add(Tag tag) {
		throw new UnsupportedOperationException("There is no tag id. Only tag name");
	}
	
	@Override
	public Tag getById(Long id) {
		throw new UnsupportedOperationException("Can't get tag by id. Only by name.");
	}
	
	@Override
	public void deleteById(Long id) {
		throw new UnsupportedOperationException("Can't delete tag by id. Only by name.");
	}
	
	@Override
	public Collection<Tag> getAll(int startPos, int offset) {
		throw new UnsupportedOperationException("There is no support for tag pagging.");
	}
	
	@Override
	public void update(Tag tag) {
		throw new UnsupportedOperationException("Can't update tag without the old tagname");
	}

}
