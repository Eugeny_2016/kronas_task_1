package com.epam.newsmng.service;

import java.util.Collection;

import com.epam.newsmng.domain.News;

/**
 * The Interface NewsService.
 */
public interface NewsService extends IService<News> {
	/*
	*//**
	 * Gets comments by news id.
	 *
	 * @param newsId the news id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return comments by news
	 *//*
	Collection<Comment> getCommentsByNewsId(long newsId, int startPos, int offset);*/

	/**
	 * Gets the news by author.
	 *
	 * @param authorName the author name
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the news by author
	 */
	Collection<News> getNewsByAuthor(Long authorId, int startPos, int offset);
	
	/**
	 * Gets the news by tag.
	 *
	 * @param tagId the tag id
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the news by tag
	 */
	Collection<News> getNewsByTag(String tagName, int startPos, int offset);
	
	/**
	 * Gets the top N commented news.
	 *
	 * @param topCommentedNewsAmount the top commented news amount
	 * @param startPos the start position
	 * @param offset the offset
	 * @return the top N commented news
	 */
	Collection<News> getTopNCommentedNews(int topCommentedNewsAmount, int startPos, int offset);
}
