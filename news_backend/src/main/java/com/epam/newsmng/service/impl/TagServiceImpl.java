package com.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.TagDAO;
import com.epam.newsmng.domain.Tag;
import com.epam.newsmng.domain.TagTO;

@Service
@Transactional
public class TagServiceImpl extends AbstractTagService {

	@Autowired
	private TagDAO tagDAO;
	
	
	@Override
	public Tag save(Tag tag) {
		return tagDAO.persist(tag);
	}

	@Override
	public Tag getByName(String tagName) {
		return tagDAO.fetchByName(tagName);
	}
	
	@Override
	public Collection<Tag> getAll() {
		return tagDAO.retrieveAll();
	}
	
	@Override
	public void update(String oldTagName, Tag newTag) {
		tagDAO.update(oldTagName, newTag);		
	}

	@Override
	public void delete(String tagName) {
		tagDAO.remove(tagName);
	}

	@Override
	public Collection<TagTO> getAmountOfNewsPerTag() {
		return tagDAO.retrieveAmountOfNewsPerTag();
	}
}
