package com.epam.newsmng.service.impl;

import java.util.Collection;

import com.epam.newsmng.domain.Author;
import com.epam.newsmng.service.AuthorService;

public abstract class AbstractAuthorService implements AuthorService {

	@Override
	public Collection<Author> getAll(int startPos, int offset) {
		throw new UnsupportedOperationException("There is no support for author pagging.");
	}

}
