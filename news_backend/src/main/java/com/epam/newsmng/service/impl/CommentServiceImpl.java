package com.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.CommentDAO;
import com.epam.newsmng.domain.Comment;
import com.epam.newsmng.service.CommentService;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDAO commentDAO;

	@Override
	public Long add(Comment comment) {
		return commentDAO.save(comment);
	}
	
	@Override
	public void update(Comment comment) {
		commentDAO.update(comment);
	}

	@Override
	public Comment getById(Long commentId) {
		return commentDAO.retrieveById(commentId);
	}

	@Override
	public Collection<Comment> getAll(int startPos, int offset) {
		return commentDAO.retrieveAll(startPos, offset);
	}

	@Override
	public void deleteById(Long commentId) {
		commentDAO.deleteById(commentId);
	}
	
	@Override
	public Collection<Comment> getCommentsByNewsId(long newsId, int startPos, int offset) {
		return commentDAO.retrieveCommentsByNewsId(newsId, startPos, offset);
	}

	@Override
	public void censureComment(Long commentId, String messageWithReason) {
		commentDAO.updateCommentWithCensuring(commentId, messageWithReason);
	}	
}
