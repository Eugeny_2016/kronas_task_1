package com.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmng.dao.AuthorDAO;
import com.epam.newsmng.domain.Author;
import com.epam.newsmng.domain.AuthorTO;

@Service
@Transactional
public class AuthorServiceImpl extends AbstractAuthorService {

	@Autowired
	private AuthorDAO authorDAO;
		
	@Override
	public Long add(Author author) {
		return authorDAO.save(author);
	}

	@Override
	public Author getById(Long authorId) {
		return authorDAO.retrieveById(authorId);
	}

	@Override
	public Collection<Author> getAll() {
		return authorDAO.retrieveAll();
	}
	
	@Override
	public void update(Author author) {
		authorDAO.update(author);
	}

	@Override
	public void deleteById(Long authorId) {
		authorDAO.deleteById(authorId);
	}

	@Override
	public Collection<AuthorTO> getAmountOfNewsPerAuthor() {
		return authorDAO.retrieveAmountOfNewsPerAuthor();
	}
}
