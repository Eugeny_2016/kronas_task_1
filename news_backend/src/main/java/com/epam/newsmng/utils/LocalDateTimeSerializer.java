package com.epam.newsmng.utils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {
	
	private static Logger LOG = Logger.getLogger(LocalDateTimeSerializer.class);
	
	@Override
	public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {
		
/*		ZonedDateTime zdt = value.atZone(ZoneId.of("Europe/Minsk"));
		LOG.error(ZoneId.systemDefault());
		LOG.error(zdt.toEpochSecond());
		LOG.error(zdt.toInstant().toEpochMilli());
		LOG.error(value.toString());
		LOG.error(Long.toString(zdt.toEpochSecond()));*/
		
		gen.writeString(value.format(DateTimeFormatter.ofPattern("MMM d, y")));
	}
	
	public static void main(String[] args) {
		
		LocalDateTime ldt = LocalDateTime.of(2014, 4, 20, 12, 40, 0);
		ZonedDateTime zdt = ldt.atZone(ZoneId.of("Europe/Minsk"));
		System.out.println(Long.toString(zdt.toEpochSecond()));
	}
}
